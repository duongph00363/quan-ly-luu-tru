# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging

from odoo import http
from odoo.http import request
from odoo.addons.auth_signup.controllers.main import AuthSignupHome
import json

_logger = logging.getLogger(__name__)


class BMSAuthSignupHome(AuthSignupHome):
    def get_auth_signup_qcontext(self):
        qcontext = super(BMSAuthSignupHome, self).get_auth_signup_qcontext()
        vn_id = request.env.ref('base.vn').id
        qcontext['state_ids'] = request.env['res.country.state'].sudo().search(
            [('country_id', '=', vn_id)])
        qcontext['district_ids'] = request.env[
            'res.country.district'].sudo().search([])
        qcontext['ward_ids'] = request.env[
            'res.country.ward'].sudo().search([])
        qcontext['management_ids'] = request.env['res.partner'].sudo().search(
            [('is_company', '=', True)])
        # qcontext['state'] = request.env.ref('bms_guest_stay.state_14')
        return qcontext

    @http.route('/get_districts/<int:state_id>',
                type='http', auth='public', website=True)
    def get_districts(self, state_id):
        districts = request.env[
            'res.country.district'].sudo().search_read(
            [('state_id', '=', state_id)], ['name'])
        res = [[d['id'], d['name']] for d in districts]
        res = json.dumps(res)
        return res

    @http.route('/get_wards/<int:district_id>', type='http', auth='public',
                website=True)
    def get_wards(self, district_id):
        wards = request.env['res.country.ward'].sudo().search_read(
            [('district_id', '=', district_id)], ['name'])
        res = [[w['id'], w['name']] for w in wards]
        res = json.dumps(res)
        return res

    @http.route('/get_managements/<int:ward_id>', type='http', auth='public',
                website=True)
    def get_managements(self, ward_id):
        managements = request.env['res.partner'].sudo().search_read(
            [('ward_id', '=', ward_id)], ['name'])
        res = [[m['id'], m['name']] for m in managements]
        res = json.dumps(res)
        return res
