odoo.define('bms_guest_stay_website.signup', function (require) {
    "use strict";

    $('.state_id').change(function(){
        var state_id = document.getElementById('state_id').value;
        var district_select = $("#district_id")[0];

        var url = "/get_districts/" + state_id;

        for(var i = district_select.options.length - 1 ; i >= 0 ; i--){
                district_select.remove(i);
            }

        $.getJSON(url,
           function(districts) {
               for (var d in districts) {
                   district_select.add(new Option(districts[d][1], districts[d][0]));
               }
               if (districts.length > 0){
                   $('#district_id').change();
               }
           });
    });

    $('#district_id').change(function(){
        var district_id = document.getElementById('district_id').value;
        var ward_select = $("#ward_id")[0];

        var url = "/get_wards/" + district_id;

        for(var i = ward_select.options.length - 1 ; i >= 0 ; i--){
                ward_select.remove(i);
            }

        $.getJSON(url,
           function(wards) {
               for (var w in wards) {
                   ward_select.add(new Option(wards[w][1], wards[w][0]));
               }
               if (wards.length > 0){
                   $('#ward_id').change();
               }
           });
    });

    $('#ward_id').change(function(){
        var ward_id = document.getElementById('ward_id').value;
        var management_select = $("#management_id")[0];

        var url = "/get_managements/" + ward_id;

        for(var i = management_select.options.length - 1 ; i >= 0 ; i--){
                management_select.remove(i);
            }

        $.getJSON(url,
           function(managements) {
               for (var m in managements) {
                   management_select.add(new Option(managements[m][1], managements[m][0]));
               }
           });
    });

});
