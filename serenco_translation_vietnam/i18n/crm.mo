��         �  {  �      �  
   �     �     �  (   �  3     8   C  9   |  O   �  E     L   L  J   �  �   �  2   �           "     5  
   <     G     P     X     ^  g   k     �     �     �     �  >     G   @     �     �     �     �     �     �     �     �     �      �       :   !     \  M   a     �  
   �  9   �     �            
        &     :  
   G     R     [     n     �     �     �     �     �     �     �     �       
     
   '     2     @     O     X     a     p  	   ~     �     �     �     �     �     �     �           
            +      2      :      G      a      f      l      x      ~      �      �      �      �      �   
   �      �   w   �      c!     p!     }!     �!     �!     �!     �!     �!  �   �!     y"     �"     �"  
   �"     �"  '   �"     �"     �"     #     #     (#     8#     =#     M#     Z#     v#     �#     �#     �#     �#     �#  �   �#     �$     �$  j   �$  0   G%     x%     ~%  D   �%     �%     �%  	   �%     �%  	   �%     &     #&  C   *&     n&     w&     }&     �&     �&     �&     �&     �&     '  !   .'     P'     \'     i'     ~'  #   �'     �'     �'     �'     �'     �'     �'     �'     �'     �'     �'     (     (  
   &(     1(     8(     =(      C(     d(  �   �(     /)     =)  >  T)  %   �*     �*     �*     �*     �*      +     +     !+     (+     0+     6+     J+     P+     W+     f+     n+     �+     �+     �+     �+  M   �+     �+     ,     ,     &,     6,     >,     G,     S,     b,     n,  	   ,     �,  
   �,     �,     �,     �,     �,     �,    �,  	   .     .     .     (.     7.     M.     a.     h.     n.  
   |.     �.     �.     �.     �.     �.  	   �.     �.     �.     �.     �.     �.     �.     �.     �.  z   /  �   �/  f   <0  6   �0  �   �0     �1     �1     �1     �1  0   �1  
   �1     �1     �1     �1     2     32  
   82  
   C2     N2     T2  K   X2     �2     �2     �2     �2  	   �2     �2     �2     �2     �2     3     3     3     (3  �  03     �4  +   �4  '   	5  H   15  1   z5  F   �5  @   �5  L   46  M   �6  J   �6  J   7  �   e7  E   X8  7   �8     �8     �8     9     9     %9     29     ;9     P9     �9     �9  	   �9  
   :  E   :  q   W:     �:  
   �:  	   �:  !   �:     ;     ;     ;     -;     <;  $   B;     g;  J   m;     �;  Y   �;     <     &<  V   3<     �<     �<     �<  	   �<     �<     �<     �<  
   �<     =     =     8=     R=     l=  
   �=     �=     �=     �=     �=     �=     �=     �=     >     >     *>     6>     C>     V>     h>     u>     �>     �>     �>     �>     �>  -   �>     (?     -?  
   H?     S?  
   `?     k?  +   }?     �?     �?     �?     �?     �?  -   �?     @      @     6@     L@     f@     w@  �   {@     A     $A  
   1A     <A     MA     RA     VA     YA  �   \A     XB  
   nB     yB     �B     �B  1   �B     �B     �B     C     C     =C     \C     iC     �C  -   �C     �C     �C     D     D     0D     ID    bD     nE  $   �E  �   �E  *   @F     kF  5   qF  p   �F     G     %G     ,G     FG  	   [G  .   eG     �G  b   �G     H     H     H  1   0H     bH     �H     �H     �H  #   �H  !   �H     I     (I     AI     WI  &   mI     �I     �I  	   �I     �I     �I     �I     �I     �I     J     J     &J     FJ     aJ     tJ     �J     �J  '   �J  %   �J  �   �J     �K     �K  ~  �K  !   wM  	   �M     �M  1   �M     �M     N     N     7N     CN     ON     UN     oN     �N     �N     �N  "   �N     �N     �N     �N     O  a   O     }O     �O     �O     �O     �O     �O     �O     �O     P     P  	   .P     8P     HP     _P     vP     �P     �P     �P  p  �P     QR     ^R     jR      {R     �R     �R     �R     �R  !   �R     S     !S  !   :S     \S     jS     wS     �S     �S  
   �S  
   �S     �S  
   �S     �S     �S     �S  �   T  *  �T  j   �U  9   JV  �   �V     XW     dW     mW     yW  I   �W     �W     �W     �W  ;   X     BX     ^X     mX     �X     �X     �X  \   �X     Y     Y     &Y     8Y     IY  $   YY     ~Y     �Y     �Y     �Y     �Y     �Y  	   �Y     �   y      <   �   �        j       ;   �   �       g           %   �   J   �              t              �   o   �   �   O   �   �   �   �          +   �   �     Z   V         �   �   �   �       !     2   �   �           U   6   �           ^   �   	  �   �               �   �   �          �   �   �   �   �     =           �       *   M     �   X      �       L       l   �   3   �       ?       �                   �   �                   �   I   q       �   
     �   E     -           �   4   �   '   )   i   �   �   �      �       �   �       [           �      �   A     5   �   b   9     a          .   �   8       �         `   �   �       ~   H   #                     �   �       �   �                  n   �              �   �             �      p      T       �     P   �          �           �   �   G   w   D       �   K   �     	       @              r   �   &   �   C   |   �       �          �          h            �       �             �       k   e   c   :       {   d   �          �   �       }   �          �   �               �   �          Q   �   �   �   �   v   
   ]   �   �       W      �   /       F   >   �       \   u   �           7   ,                 �   B   m      �   �   f   �   s         �   �   $   �   �   �       �   �   �   _   �      �     �   �   �         �             "           �   �   �   �      z      1   �   �   R       x     �   N   �   (   0             �       S   Y   �    # Meetings (if leads are activated) (you can change it here) , if accounting or purchase is installed <i class="fa fa-question-circle"> view examples</i> <span class="fa fa-arrow-circle-o-down"/> Install Events <span class="fa fa-arrow-circle-o-down"/> Install Mailing <span class="label label-default text-center odoo_purple">Incoming email</span> <span class="label label-default text-center odoo_purple">Lead</span> <span class="label label-default text-center odoo_purple">Opportunity</span> <span class="label label-default text-center odoo_purple">Quotation</span> <span class="panel-title">
                                            <span class="fa fa-sitemap"/>
                                            <strong> Workflow Customization</strong>
                                        </span> A good pipeline should have between 5 and 7 stages A user associated to the contact Accept Emails From Active Activities Activity Address Alias Alias Domain All contacts must have the same email. Only the Administrator can merge contacts with different emails. Analysis Apply deduplication Archive Archived Are you sure to execute the automatic merge of your contacts ? Are you sure to execute the list of automatic merges of your contacts ? Assign opportunities to Assignation Date Assigned Automatic Merge Wizard CRM Call Call for Demo Campaign Cancel Change Probability Automatically Channel Check this box to manage opportunities in this sales team. City Classify and analyze your lead/opportunity categories like: Training, Service Close Close Date Closed/Dead leads cannot be converted into opportunities. Color Index Company Company Name Consulting Contact Information Contact Name Contact Us Contacts Conversion Options Convert to Opportunities Convert to Opportunity Convert to opportunities Convert to opportunity Country Create Create Customers Create Date Create Opportunity Create Vendors Created by Created on Creation Date Creation Month Currency Customer Customer Email Customer Name Customers Date Days to Assign Days to Close Deduplicate Contacts Deduplicate the other Contacts Default Default Alias Name for Leads Delete Describe the lead... Description Design Discard Display Name Do not link to a customer Edit Email Email Alias Event Examples Exclude Opt Out Expected Closing Expected Revenue Expected Revenues Extended Filters Extra Info Fax For safety reasons, you cannot merge more than 3 contacts together. You can re-open the wizard several times if needed. From %s : %s Gamification Group By Group of Contacts Hidden High ID Ids If opt-out is checked, this contact has refused to receive emails for mass mailing and marketing campaign. Filter 'Available for Mass Mailing' allows users to filter the leads when performing mass mailing. Import & Synchronize Information Internal Notes Is Company Job Position Journal Items associated to the contact Last Action Last Modified on Last Stage Update Last Updated by Last Updated on Lead Lead / Customer Lead Created Lead To Opportunity Partner Lead created Lead/Opportunity Leads Leads & Opportunities Leads / Opportunities Leads Analysis Leads Analysis allows you to check different CRM related information like the treatment delays or number of leads per state. You can sort out your leads analysis by different groups to get accurate grained analysis. Leads Generation Leads that are assigned to me Leads that you selected that have duplicates. If the list is empty, it means that no duplicates were found Leads with existing duplicates (for information) Lines Link to an existing customer Linked partner (optional). Usually created when converting the lead. Lost Low Mark Lost Mark Won Marketing Maximum of Group of Contacts Medium Meeting scheduled at '%s'<br> Subject: %s <br> Duration: %s hour(s) Meetings Merge Merge Automatically Merge Automatically all process Merge Leads/Opportunities Merge leads/opportunities Merge opportunities Merge the following contacts Merge with Manual Check Merge with existing opportunities Merged lead Merged leads Merged opportunities Merged opportunity Merged with the following partners: Misc Mobile Model Month My Leads My Opportunities Name Negotiation New Next Action Next Activities Next Activity No Subject Normal Note Notes Number of Days to close the case Number of Days to open the case Only the destination contact may be linked to existing Journal Items. Please ask the Administrator if you need to merge several contacts linked to existing Journal Items. Opportunities Opportunities Analysis Opportunities Analysis gives you an instant access to your opportunities with information such as the expected revenue, planned cost, missed deadlines or the number of interactions per opportunity. This report is mainly used by the sales manager in order to do the periodic review with the teams of the sales pipeline. Opportunities that are assigned to me Opportunity Opportunity Lost Opportunity Stage Changed Opportunity Won Opportunity lost Opportunity won Option Options Other Overpassed Deadline Owner Parent Parent Company Partner Partner Contact Email Partner Contact Name Partners Phone Pipeline Please select more than one element (lead or opportunity) from the list view. Previous Activity Priority Probability Probability (%) Product Products Proposition Quotation sent Referred By Related Customer Reporting Requirements Sales Team Salesmen Salesperson Search Leads Search Opportunities Select Leads/Opportunities Select the list of fields used to search for
                            duplicated records. If you select several fields,
                            Odoo will propose you to merge only those having
                            all these fields in common. (not one of the fields). Selection Sequence Services Show only lead Show only opportunity Skip these contacts Source Stage Stage Changed Stage Name Stage Search Stage changed Stages State Street Street... Street2 Summary Tag Tag name already exists ! Tags Task Team Team Activities The email address associated with this team. New emails received will automatically create new leads assigned to the team. The first contact you get with a potential customer is a lead you qualify before converting it into a real business opportunity. Check this box to manage leads in this sales team. The name of the future partner company that will be created while converting the lead into opportunity There is no more contacts to merge for this request... These email addresses will be added to the CC field of all inbound and outbound emails for this record before being sent. Separate multiple email addresses with a comma Title Tracking Training Type Type is used to separate Leads and Opportunities Unassigned Unread Messages Update Date Use existing partner or create Used to log into the system User User Email User Login Users VAT When sending mails, the default email address is taken from the sales team. Wizard Won Working Time Your Expectations Your KPIs Your Pipeline Your business cards ZIP Zip opportunities or sale.config.settings unknown Project-Id-Version: Odoo Server 10.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-04-09 11:10+0700
Last-Translator: Su Pham Nghiep Vu <nghiepvusupham58@gmail.com>, 2017
Language-Team: Vietnamese (https://www.transifex.com/odoo/teams/41243/vi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: vi
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.0.4
 Các cuộc gặp (nếu các đầu mối đã kích hoạt) (bạn có thể thay đổi ở đây) , nếu phân hệ kế toán hoặc mua hàng đã được cài đặt <i class="fa fa-question-circle">xem ví dụ</i> <span class="fa fa-arrow-circle-o-down"/> Cài đặt các sự kiện <span class="fa fa-arrow-circle-o-down"/> Cài đặt gửi thư <span class="label label-default text-center odoo_purple">Thư đến</span> <span class="label label-default text-center odoo_purple">Đầu mối</span> <span class="label label-default text-center odoo_purple">Cơ hội</span> <span class="label label-default text-center odoo_purple">Báo giá</span> <span class="panel-title">
                                            <span class="fa fa-sitemap"/></span>
                                            <strong> Tuỳ chỉnh quy trình</strong>
                                        </span> Đường dẫn pipeline nên ở trong khoảng 5 và 7 giai đoạn Một người dùng liên kết tới một liên hệ Chấp nhận email đến Có hiệu lực Các hoạt động Hoạt động Địa chỉ Bí danh Tên miền bí danh Tất cả liên hệ phải có cùng email. Chỉ Quản trị viên có thể trộn liên hệ với các email khác nhau. Phân tích Áp dụng chống trùng lặp Đã lưu Đã lưu  Bạn chắc chắn trộn tự động các liên hệ của mình ? Bạn chắc chắn thực hiện một danh sách công việc trộn tự động các liên hệ của mình ? Giao cơ hội cho Ngày giao Đã giao Đồ thuật trộn tự động CRM Gọi Yêu cầu Demo Chiến dịch Hủy Tự động thay đổi xác suất Kênh Đánh dấu để quản lý cơ hội trong đội ngũ bán hàng này. Thành phố Phân loại và phân tích nhóm nguồn dẫn/cơ hội như: Đào tạo, Dịch vụ Đóng Ngày đóng Nguồn dẫn đã đóng/kết thúc không thể được chuyển thành cơ hội. Mã màu Công ty Tên Công ty Tư vấn Thông tin liên hệ Tên liên hệ Liên hệ với chúng tôi Liên hệ Tùy chọn chuyển đổi Chuyển thành cơ hội Chuyển thành Cơ hội Chuyển thành cơ hội Chuyển thành Cơ hội Quốc gia Tạo Tạo danh sách khách hàng Create Date Tạo Cơ hội Tạo Nhà cung cấp Được tạo bởi Thời điểm tạo Ngày tạo Tháng tạo Tiền tệ Khách hàng Email khách hàng Tên Khách hàng Khách hàng Ngày tháng Số ngày phân công Ngày để đóng Chống trùng lặp liên hệ Chống trùng lặp liên hệ Mặc định Tên bí danh mặc định cho Nguồn dẫn Xóa Miêu tả đầu mối... Miêu tả Thiết kế Hủy bỏ Tên hiển thị Không liên kết tới một khách hàng Sửa Email Email bí danh Sự kiện Ví dụ Loại trừ đối tác đánh dấu Opt-Out Ngày đóng dự kiến Doanh thu mong đợi Doanh thu mong đợi Bộ lọc mở rộng... Thông tin thêm Fax Vì lý do an toàn, bạn không thể trộn nhiều hơn 3 liên hệ cùng nhau. Bạn có thể mở lại đồ thuật một vài lần nếu cần. Từ %s : %s Gamification Nhóm theo Nhóm liên hệ Ẩn Cao ID ID Nếu trường này được đánh dấu, liên hệ này sẽ từ chối nhận email hàng loạt. Bộ lọc 'Có khả năng gửi email hàng loạt' cho phép người dùng lọc các nguồn dẫn khi thực hiện gửi email hàng loạt. Nhập & Đồng bộ Thông tin Ghi chú nội bộ Là công ty Chức vụ Phát sinh kế toán liên quan tới liên hệ Hành động cuối cùng Sửa lần cuối vào Last Stage Update Cập nhật lần cuối bởi Cập nhật lần cuối vào Đầu mối Đầu mối / Khách hàng Đầu mối được tạo Từ Đầu mối tới cơ hội đối tác Đầu mối được tạo Đầu mối/Cơ hội Các đầu mối Nguồn dẫn và Cơ hội Đầu mối / Cơ hội Phân tích Đầu Mối Phân tích Đầu mối cho phép ạbạn kiểm tra thông tin Quản trị quan hệ khách hàng khác nhau như là việc xử lý chậm trễ hoặc số nguồn dẫn theo từng bước. Bạn có thể sắp xếp nguồn dẫn theo các nhóm khác nhau. Tạo nguồn dẫn Đầu mối được giao cho bạn Đầu mối mà bạn chọn có sự trùng lặp. Nếu danh sách trống, nó có nghĩa là không có sự trùng lặp nào được tìm thấy Đầu mối với thông tin trùng lặp Lines Liên kết tới một khách hàng đã tồn tại Đối tác được liên kết (tùy chọn). Thường được tạo khi chuyển đổi một Đầu mối  Thất bại Thấp Đánh dấu thất bại Đánh dấu thắng Marketing Số lượng tối đa của nhóm liên hệ Trung bình Cuộc họp được đặt lịch tại '%s'<br> Chủ đề: %s <br> Trong khoảng: %s hour(s) Các cuộc gặp Trộn Trộn tự động Trộn tự động tất cả các tiến trình Trộn Đầu mối/Cơ hội Trộn Đầu mối/Cơ hội Gộp các cơ hội Trộn các liên hệ sau Trộn với kiểm tra thủ công Trộn cơ hội đã tồn tại Trộn Đầu mối Đầu mối đã trộn Cơ hội đã trộn Cơ hội đã trộn Được trộn với đối tác sau: Các thứ khác Số di động Mô hình Tháng Đầu mối của bạn Cơ hội của bạn Tên Đàm phán Mới Hành động tiếp theo Các Hoạt động tiếp theo Hoạt động tiếp theo Không chủ đề Thông thường Ghi chú Ghi chú Số ngày để đóng trường hợp Số ngày để mở trường hợp Chỉ liên hệ đến (destination contact) mới có thể liên kết tới một phát sinh đã tồn tại. Vui lòng hỏi Quản trị viên nếu bạn cần trộng một vài liên hệ tới các phát sinh đã tồn tại. Các cơ hội Phân tích Cơ hội Bản phân tích Cơ hội cho phép bạn truy cập nhanh đến các cơ hội với thông tin như: doanh thu mong đợi, chi phí dự kiến, số lần tương tác,... Báo cáo này chủ yếu được sử dụng bởi Người quản lý bán hàng để thực hiện việc quản lý định kỳ với các đội ngũ bán hàng của một chu trình bán hàng Cơ hội được giao cho bạn Cơ hội Cơ hội thất bại Giai đoạn của cơ hội được thay đổi Cơ hội chiến thắng Cơ hội thất bại Cơ hội chiến thắng Tùy chọn Tùy chọn Khác Vượt quá thời hạn Chủ sở hữu Dự án cha Công ty mẹ Đối tác Email liên hệ của đối tác Tên liên hệ đối tác Đối tác Điện thoại Đường dẫn Pipeline Vui lòng chọn nhiều hơn một phần tử (nguồn dẫn hoặc cơ hội) từ danh sách. Hoạt động trước Mức độ ưu tiên Xác suất Xác suất (%) Sản phẩm Sản phẩm Đề xuất Báo giá đã gửi Tham chiếu bởi Khách hàng liên quan Báo cáo Các yêu cầu Đội ngũ bán hàng Nhân viên bán hàng Nhân viên bán hàng Tìm kiếm Đầu mối Tìm kiếm các Cơ hội Chọn  Đầu mối/Cơ hội Chọn một danh sách các trường được sử dụng để tìm kiếm cho
                            các bản ghi bị trùng lặp. Nếu bạn chọn một nhiều trường,
                            hệ thống sẽ đề xuất bạn trộn những bản ghi có chung dữ liệu cho tất cả các trường (không phải chỉ một trường). Lựa chọn Trình tự Các dịch vụ Chỉ hiển thị  Đầu mối Chỉ hiển thị cơ hội Bỏ qua các liên hệ này Nguồn Trạng thái Giai đoạn được thay đổi Tên giai đoạn Tìm kiếm giai đoạn Giai đoạn được thay đổi Trạng thái Giai đoạn Địa chỉ Địa chỉ... Địa chỉ Tóm tắt Từ khóa Nội dung này đã có rồi! Từ khóa Nhiệm vụ Thành viên Hoạt động Địa chỉ email được liên kết với đội ngũ này. Email mới nhận được sẽ tự động tạo Đầu mối mới và gắn tới đội ngũ này. Liên hệ đầu tiên bạn nhận được với một khách hàng tiềm năng là một nguồn dẫn mà bạn cần thẩm định trước khi chuyển nó thành một cơ hội kinh doanh thực sự. Đánh dấu ô này để quản lý nguồn dẫn trong đội ngũ bán hàng này. Tên công ty của khách hàng mà sẽ được tạo trong khi chuyển nguồn dẫn thành cơ hội Không có liên hệ để trộn cho yêu cầu này... Các địa chỉ email sẽ được thêm vào trường CC của tất cả các email đi, email đến cho bản ghi này trước khi gửi. Mỗi địa chỉ email được ngăn cách bởi dấu phẩy Tiêu đề Tracking Đào tạo Loại Loại được sử dụng để tách riêng đầu mối và Cơ hội Chưa được gán Thông điệp chưa đọc Update Date Sử dụng đối tác đã tồn tại hoặc tạo mới Used to log into the system Người dùng Email người dùng Người dùng đăng nhập Người dùng Thuế GTGT Khi gửi mail, địa chỉ email mặc định được lấy từ đội ngũ bán hàng. Wizard Thắng Giờ làm việc Các kỳ vọng Chỉ số KPIs Đường dẫn pipeline của bạn Danh thiếp  ZIP Mã bưu chính Các cơ hội hoặc sale.config.settings chưa rõ 