# -*- coding: utf-8 -*-
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
import datetime
from odoo import fields, models, tools


class ReportNotebookOfStay(models.Model):
    _name = 'report.notebook.of.stay'
    _auto = False

    name = fields.Char()
    birthday = fields.Date()
    gender = fields.Selection([('male', 'Male'),
                               ('female', 'Female'),
                               ('undefine', 'Un define')],
                              default='male')
    identification_number = fields.Char()
    job = fields.Char()
    nation = fields.Char()
    country_id = fields.Many2one('res.country')
    # national = fields.Char()
    state_id = fields.Many2one('res.country.state',
                               string='State')
    district_id = fields.Many2one('res.country.district',
                                  string='District')
    ward_id = fields.Many2one('res.country.ward',
                              string='Ward')
    partner_id = fields.Many2one('res.partner')
    guest_address = fields.Char()
    reason_id = fields.Many2one('bms.guest.reason')
    reason = fields.Char()
    from_date = fields.Datetime()
    to_date = fields.Datetime()
    accommodation_address = fields.Char()
    accommodation_id = fields.Many2one('bms.accommodation',
                                       string='Accommodation')
    accommodation_name = fields.Char()
    user_id = fields.Many2one('res.users')
    police = fields.Char()
    note = fields.Text()

    def init(self):
        tools.drop_view_if_exists(self._cr, self._table)
        self._cr.execute(
            """CREATE or REPLACE view report_notebook_of_stay as (
                select
                 g.id as id,
                 g.name as name,
                 g.birth_day as birthday,
                 g.gender as gender,
                 g.identification_number as identification_number,
                 '' as job,
                 g.nation as nation,
                 g.country_id as country_id,
                 rs.id as state_id,
                 rd.id as district_id,
                 rw.id as ward_id,
                 coalesce(g.street || ', ' , '') ||
                 coalesce(rwg.name || ', ' , '') ||
                 coalesce(rdg.name || ', ' , '') ||
                 coalesce(rsg.name, '') as guest_address,
                 br.id as reason_id,
                 br.name as reason,
                 g.from_date as from_date,
                 g.to_date as to_date,
                 coalesce(arw.name || ', ' , '') ||
                 coalesce(ard.name|| ', ', '')||
                 coalesce(ars.name, '') as accommodation_address,
                 a.partner_id as partner_id,
                 a.id as accommodation_id,
                 a.name as accommodation_name,
                 '' as police,
                 g.note as note,
                 ru.id as user_id
                from bms_guest_stay as g
                 left join bms_accommodation
                 as a on g.accommodation_id = a.id
                 left join res_partner
                 as p on p.id = a.partner_id
                 left join res_country
                 as rc on rc.id = p.country_id
                 left join res_country_state
                 as rs on rs.id = p.state_id
                 left join res_country_district
                 as rd on rd.id = p.district_id
                 left join res_country_ward
                 as rw on rw.id = p.ward_id
                 left join bms_guest_reason
                 as br on br.id = g.reason
                 left join res_country_state
                 as ars on ars.id = a.state_id
                 left join res_country_district
                 as ard on ard.id = a.district_id
                 left join res_country_ward
                 as arw on arw.id = a.ward_id
                 left join res_country_state
                 as rsg on rsg.id = g.state_id
                 left join res_country_district
                 as rdg on rdg.id = g.district_id
                 left join res_country_ward
                 as rwg on rwg.id = g.ward_id
                 left join res_users
                 as ru on ru.id = g.user_id
                )
            """
        )


class ReportNotebookOfStayXlsx(ReportXlsx):
    def generate_xlsx_report(self, workbook, data, wizards):
        report_name = str(wizards.id)
        sheet = workbook.add_worksheet(report_name[:31])

        header_bold = workbook.add_format({'bold': True})
        header_bold.set_text_wrap()
        header_bold.set_align('center')
        header_bold.set_font_name('Times New Roman')
        header_bold.set_bottom()
        header_bold.set_top()
        header_bold.set_left()
        header_bold.set_right()
        header_bold.set_font_size(9)
        header_bold.set_fg_color('green')

        bold = workbook.add_format({'bold': True})
        bold.set_text_wrap()
        bold.set_align('center')
        bold.set_font_name('Times New Roman')
        bold.set_font_size(11)

        border_bold = workbook.add_format({'bold': True})
        border_bold.set_text_wrap()
        border_bold.set_align('center')
        border_bold.set_font_name('Times New Roman')
        border_bold.set_bottom()
        border_bold.set_top()
        border_bold.set_left()
        border_bold.set_right()
        border_bold.set_fg_color('gray')

        red_bold_left = workbook.add_format({'bold': True})
        red_bold_left.set_text_wrap()
        red_bold_left.set_align('left')
        red_bold_left.set_font_name('Times New Roman')
        red_bold_left.set_bottom()
        red_bold_left.set_top()
        red_bold_left.set_left()
        red_bold_left.set_right()
        red_bold_left.set_fg_color('pink')

        orange_bold_center = workbook.add_format({'bold': True})
        orange_bold_center.set_text_wrap()
        orange_bold_center.set_align('center')
        orange_bold_center.set_font_name('Times New Roman')
        orange_bold_center.set_bottom()
        orange_bold_center.set_top()
        orange_bold_center.set_left()
        orange_bold_center.set_right()
        orange_bold_center.set_fg_color('yellow')

        orange_bold_left = workbook.add_format({'bold': True})
        orange_bold_left.set_text_wrap()
        orange_bold_left.set_align('left')
        orange_bold_left.set_font_name('Times New Roman')
        orange_bold_left.set_bottom()
        orange_bold_left.set_top()
        orange_bold_left.set_left()
        orange_bold_left.set_right()
        orange_bold_left.set_fg_color('yellow')

        normal = workbook.add_format({})
        normal.set_text_wrap()
        normal.set_align('left')
        normal.set_font_name('Times New Roman')
        normal.set_bottom()
        normal.set_top()
        normal.set_left()
        normal.set_right()
        normal.set_align('vjustify')

        normal_normal = workbook.add_format({})
        normal_normal.set_font_name('Times New Roman')

        normal_normal_note = workbook.add_format({})
        normal_normal_note.set_font_name('Times New Roman')
        normal_normal_note.set_font_size(9)

        bold_1 = workbook.add_format({'bold': True})
        bold_1.set_font_name('Times New Roman')
        bold_1.set_font_size(9)

        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 16)
        sheet.set_column('E:E', 16)
        sheet.set_column('F:F', 16)
        sheet.set_column('G:G', 16)
        sheet.set_column('H:H', 16)
        sheet.set_column('I:I', 38)
        sheet.set_column('J:J', 16)
        sheet.set_column('K:K', 20)
        sheet.set_column('L:L', 20)
        sheet.set_column('M:M', 38)
        sheet.set_column('N:N', 30)
        sheet.set_column('O:O', 12)
        sheet.set_column('P:P', 12)

        row_pos = 1
        sheet.merge_range('A{}:C{}'.format(row_pos, row_pos), u'BỘ CÔNG AN',
                          bold)
        sheet.write(0, 8, u'Mẫu HK13 ban hành', normal_normal)

        row_pos += 1
        # sheet.merge_range('A{}:C{}'.format(row_pos, row_pos),
        #                   u'TÀI KHOẢN THỬ NGHIỆM',
        #                   bold)
        sheet.write(1, 8, u'theo TT số', normal_normal)
        sheet.write(2, 8, u'36/2014/TT-BCA', normal_normal)
        sheet.write(3, 8, u'ngày 09/9/2014', normal_normal)

        row_pos += 3
        sheet.merge_range('A{}:I{}'.format(row_pos, row_pos),
                          u'SỔ TIẾP NHẬN LƯU TRÚ', bold)
        row_pos += 1
        sheet.merge_range('D{}:G{}'.format(row_pos, row_pos),
                          u'XÃ/PHƯỜNG/THỊ TRẤN..............', normal_normal)

        row_pos += 1
        sheet.merge_range('D{}:G{}'.format(row_pos, row_pos),
                          u'{}'.format(wizards.management_unit_id.name or ''),
                          normal_normal)

        row_pos += 1
        sheet.merge_range('G{}:H{}'.format(row_pos, row_pos),
                          u'QUYỂN SỐ:……………', normal_normal)

        row_pos += 1
        from_date_label = u'-Bắt đầu ngày: {}'.format(
            datetime.datetime.strptime(wizards.from_date,
                                       '%Y-%m-%d').strftime(
                '%d-%m-%Y'))
        sheet.merge_range('G{}:H{}'.format(row_pos, row_pos), from_date_label,
                          normal_normal)

        row_pos += 1
        to_date_label = u'-Kết thúc ngày: {}'.format(
            datetime.datetime.strptime(wizards.to_date,
                                       '%Y-%m-%d').strftime(
                '%d-%m-%Y'))
        sheet.merge_range('G{}:H{}'.format(row_pos, row_pos), to_date_label,
                          normal_normal)

        row_pos += 1
        sheet.merge_range('G{}:I{}'.format(row_pos, row_pos),
                          u'(1) Công an quận/huyện/thị xã/TP thuộc tỉnh',
                          normal_normal)

        row_pos += 1
        sheet.merge_range('G{}:I{}'.format(row_pos, row_pos),
                          u'(2) Điểm tiếp nhận lưu trú',
                          normal_normal)

        row_pos += 3
        sheet.merge_range('A{}:A{}'.format(row_pos, row_pos + 1),
                          u'STT', header_bold)
        sheet.merge_range('B{}:B{}'.format(row_pos, row_pos + 1),
                          u'HỌ VÀ TÊN', header_bold)
        sheet.merge_range('C{}:C{}'.format(row_pos, row_pos + 1),
                          u'NGÀY, THÁNG, NĂM SINH', header_bold)
        sheet.merge_range('D{}:D{}'.format(row_pos, row_pos + 1),
                          u'GIỚI TÍNH', header_bold)
        sheet.merge_range('E{}:E{}'.format(row_pos, row_pos + 1),
                          u'CMND SỐ HOẶC SỐ HỘ CHIẾU', header_bold)
        sheet.merge_range('F{}:F{}'.format(row_pos, row_pos + 1),
                          u'NGHỀ NGHIỆP, NƠI LÀM VIỆC', header_bold)
        sheet.merge_range('G{}:G{}'.format(row_pos, row_pos + 1),
                          u'DÂN TỘC', header_bold)
        sheet.merge_range('H{}:H{}'.format(row_pos, row_pos + 1),
                          u'QUỐC TỊCH', header_bold)
        sheet.merge_range('I{}:I{}'.format(row_pos, row_pos + 1),
                          u'NƠI THƯỜNG TRÚ HOẶC TẠM TRÚ', header_bold)
        sheet.merge_range('J{}:J{}'.format(row_pos, row_pos + 1),
                          u'LÝ DO LƯU TRÚ', header_bold)
        sheet.merge_range('K{}:L{}'.format(row_pos, row_pos),
                          u'THỜI GIAN LƯU TRÚ', header_bold)
        sheet.write(row_pos, 10,
                    u'Đến ngày', header_bold)
        sheet.write(row_pos, 11,
                    u'Đi ngày', header_bold)
        # sheet.merge_range('M{}:M{}'.format(row_pos, row_pos + 1),
        #                   u'ĐỊA CHỈ LƯU TRÚ (2) '
        #                   u'(ghi rõ số nhà, đường phố, tổ,
        # thôn, xóm, làng, ấp'
        #                   u', bản, buôn, phum, sóc)',
        #                   header_bold)
        sheet.merge_range('M15:M16', u'', header_bold)
        sheet.write_rich_string('M15', bold_1, u'ĐỊA CHỈ LƯU TRÚ',
                                normal_normal_note,
                                u'(2)(ghi rõ số nhà, đường phố, tổ, thôn, '
                                u'xóm, làng, ấp, bản, buôn, phum, sóc)',
                                header_bold)

        sheet.merge_range('N{}:N{}'.format(row_pos, row_pos + 1),
                          u'TÊN CƠ SỞ LƯU TRÚ', header_bold)
        sheet.merge_range('O{}:O{}'.format(row_pos, row_pos + 1),
                          u'CÔNG AN TIẾP NHẬN', header_bold)
        sheet.merge_range('P{}:P{}'.format(row_pos, row_pos + 1),
                          u'GHI CHÚ', header_bold)

        row_pos += 1
        for i in range(0, 16):
            sheet.write(row_pos, i, '({})'.format(i + 1), orange_bold_center)

        sheet.freeze_panes(17, 16)
        row_pos += 1

        datas = wizards.get_datas()
        gender_tr_selection = \
            wizards.get_tr_selection('gender')
        count_index = 0
        for data in datas:
            count_index += 1
            # gender = wizards.get_selection_value('gender',
            #                                      data['gender']) or ''
            gender = gender_tr_selection.get(data['gender'], '')
            sheet.write(row_pos, 0, count_index or '', normal)
            sheet.write(row_pos, 1, data.name or '', normal)
            sheet.write(row_pos, 2, data.birthday or '', normal)
            # sheet.write(row_pos, 3, gender or '', normal)
            sheet.write(row_pos, 3, gender, normal)
            sheet.write(row_pos, 4, data.identification_number or '', normal)
            sheet.write(row_pos, 5, data.job or '', normal)
            sheet.write(row_pos, 6, data.nation or '', normal)
            sheet.write(row_pos, 7, data.country_id.name or '', normal)
            sheet.write(row_pos, 8, data.guest_address or '', normal)
            sheet.write(row_pos, 9, data.reason or '', normal)
            sheet.write(row_pos, 10, data.from_date or '', normal)
            sheet.write(row_pos, 11, data.to_date or '', normal)
            sheet.write(row_pos, 12, data.accommodation_address or '', normal)
            sheet.write(row_pos, 13, data.accommodation_name or '', normal)
            sheet.write(row_pos, 14, data.police or '', normal)
            sheet.write(row_pos, 15, data.note or '', normal)
            row_pos += 1

        row_pos += 2
        sheet.write(row_pos, 11, u'NGƯỜI BÁO CÁO', bold)
        row_pos += 1
        sheet.merge_range('N{}:O{}'.format(row_pos, row_pos),
                          u'THỦ TRƯỞNG ĐƠN VỊ', bold)


ReportNotebookOfStayXlsx('report.bms_guest_stay.notebook_of_stay_xls.xlsx',
                         'wizard.report.notebook.of.stay')
