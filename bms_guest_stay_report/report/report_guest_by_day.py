# -*- coding: utf-8 -*-
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
import datetime
from odoo import fields, models, tools


class ReportGuestbyDay(models.Model):
    _name = 'report.guest.by.day'
    _auto = False

    name = fields.Char()
    accommodation_id = fields.Many2one('bms.accommodation',
                                       string='Accommodation')
    accommodation_name = fields.Char()
    partner_id = fields.Many2one('res.partner',
                                 string='Management unit')
    phone = fields.Char()
    street = fields.Char()
    state_id = fields.Many2one('res.country.state',
                               string='State')
    state_name = fields.Char()
    district_name = fields.Char()
    district_id = fields.Many2one('res.country.district',
                                  string='District')
    ward_name = fields.Char()
    ward_id = fields.Many2one('res.country.ward',
                              string='Ward')
    from_date = fields.Datetime()
    to_date = fields.Datetime()

    def init(self):
        tools.drop_view_if_exists(self._cr, self._table)
        self._cr.execute(
            """CREATE or REPLACE view report_guest_by_day as (
                select
                  g.id as id,
                  g.name as name,
                  g.accommodation_id as accommodation_id,
                  a.name as accommodation_name,
                  a.partner_id as partner_id,
                  a.phone as phone,
                  p.street as street,
                  p.state_id as state_id,
                  rs.name as state_name,
                  rd.name as district_name,
                  p.district_id as district_id,
                  rw.name as ward_name,
                  p.ward_id as ward_id,
                  g.from_date,
                  g.to_date
                from bms_guest_stay as g
                    left join bms_accommodation
                    as a on g.accommodation_id = a.id
                    left join res_partner as p on p.id = a.partner_id
                    left join res_country_state as rs on rs.id = p.state_id
                    left join res_country_district
                    as rd on rd.id = p.district_id
                    left join res_country_ward as rw on rw.id = p.ward_id
                )
            """
        )


class ReportGuestByDayXlsx(ReportXlsx):
    def generate_xlsx_report(self, workbook, data, wizards):
        report_name = str(wizards.id)
        sheet = workbook.add_worksheet(report_name[:31])

        header_bold = workbook.add_format({'bold': True})
        header_bold.set_text_wrap()
        header_bold.set_align('center')
        header_bold.set_font_name('Times New Roman')
        header_bold.set_bottom()
        header_bold.set_top()
        header_bold.set_left()
        header_bold.set_right()
        header_bold.set_font_size(19)
        header_bold.set_fg_color('green')

        bold = workbook.add_format({'bold': True})
        bold.set_text_wrap()
        bold.set_align('center')
        bold.set_font_name('Times New Roman')
        bold.set_font_size(11)

        border_bold = workbook.add_format({'bold': True})
        border_bold.set_text_wrap()
        border_bold.set_align('center')
        border_bold.set_font_name('Times New Roman')
        border_bold.set_bottom()
        border_bold.set_top()
        border_bold.set_left()
        border_bold.set_right()
        border_bold.set_fg_color('gray')

        red_bold_left = workbook.add_format({'bold': True})
        red_bold_left.set_text_wrap()
        red_bold_left.set_align('left')
        red_bold_left.set_font_name('Times New Roman')
        red_bold_left.set_bottom()
        red_bold_left.set_top()
        red_bold_left.set_left()
        red_bold_left.set_right()
        red_bold_left.set_fg_color('pink')

        orange_bold_left = workbook.add_format({'bold': True})
        orange_bold_left.set_text_wrap()
        orange_bold_left.set_align('left')
        orange_bold_left.set_font_name('Times New Roman')
        orange_bold_left.set_bottom()
        orange_bold_left.set_top()
        orange_bold_left.set_left()
        orange_bold_left.set_right()
        orange_bold_left.set_fg_color('yellow')

        normal = workbook.add_format({})
        normal.set_text_wrap()
        normal.set_align('left')
        normal.set_font_name('Times New Roman')
        normal.set_bottom()
        normal.set_top()
        normal.set_left()
        normal.set_right()

        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 26)
        sheet.set_column('C:C', 38)
        sheet.set_column('D:D', 16)
        sheet.set_column('E:E', 16)

        accommodation_dict, state_dict, district_dict, ward_dict \
            = wizards.get_accommodation_dict()

        row_pos = 1

        sheet.merge_range('A{}:B{}'.format(row_pos, row_pos), u'BỘ CÔNG AN',
                          bold)
        sheet.merge_range('D{}:E{}'.format(row_pos, row_pos),
                          u'CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM', bold)
        row_pos += 1

        sheet.merge_range('D{}:E{}'.format(row_pos, row_pos),
                          u'Độc lập - Tự do - Hạnh phúc', bold)
        row_pos += 2

        header_label = u'THỐNG KÊ SỐ LƯỢT KHÁCH NGÀY {} đến {}'.format(
            datetime.datetime.strptime(wizards.from_date, '%Y-%m-%d').strftime(
                '%d-%m-%Y'),
            datetime.datetime.strptime(wizards.to_date, '%Y-%m-%d').strftime(
                '%d-%m-%Y'),
        )
        sheet.merge_range('A{}:E{}'.format(row_pos, row_pos + 1), header_label,
                          header_bold)
        row_pos += 3

        col_pos = 0
        for label in ['STT', u'TÊN CƠ SỞ LƯU TRÚ', u'ĐỊA CHỈ', u'ĐIÊN THOẠI',
                      u'SỐ LƯỢT KHÁCH']:
            sheet.write(row_pos, col_pos, label, border_bold)
            sheet.write(row_pos + 1, col_pos, '({})'.format(col_pos + 1),
                        border_bold)
            col_pos += 1

        sheet.freeze_panes(9, 5)

        row_pos += 3
        for state_id in accommodation_dict.keys():
            state = state_dict.get(state_id, {})
            state_name = state.get('name', '')
            state_count = state.get('count', 0)
            sheet.merge_range('A{}:E{}'.format(row_pos, row_pos),
                              u'{}({})'.format(state_name, state_count),
                              orange_bold_left)
            row_pos += 1

            for district_id in accommodation_dict[state_id].keys():
                district = district_dict.get(district_id, {})
                district_name = district.get('name', '')
                district_count = district.get('count', 0)
                sheet.merge_range('A{}:E{}'.format(row_pos, row_pos),
                                  u'{}({})'.format(district_name,
                                                   district_count),
                                  red_bold_left)
                row_pos += 1

                for ward_id in \
                        accommodation_dict[state_id][district_id].keys():
                    ward = ward_dict.get(ward_id, {})
                    ward_name = ward.get('name', '')
                    ward_count = ward.get('count', 0)
                    sheet.merge_range('A{}:E{}'.format(row_pos, row_pos),
                                      u'{}({})'.format(ward_name, ward_count),
                                      orange_bold_left)
                    # row_pos += 1

                    col = 0
                    for accommodation in \
                            accommodation_dict[state_id][district_id][ward_id]:
                        col += 1
                        sheet.write(row_pos, 0, col, normal)
                        sheet.write(row_pos, 1,
                                    accommodation['accommodation_name'],
                                    normal)

                        address = u'{}'.format(
                            accommodation['accommodation_address'] or ''
                        )
                        sheet.write(row_pos, 2, address, normal)
                        sheet.write(row_pos, 3, accommodation['mobile'] or '',
                                    normal)
                        sheet.write(row_pos, 4, accommodation['count_guest'],
                                    normal)
                        row_pos += 1
                    row_pos += 1


ReportGuestByDayXlsx('report.bms_guest_stay.guest_by_day_xls.xlsx',
                     'wizard.report.guest.by.day')
