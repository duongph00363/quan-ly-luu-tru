# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
from odoo import tools
from datetime import datetime


class ReportGuestInday(models.Model):
    _name = 'report.guest.inday'
    _auto = False

    name = fields.Char(string='Accommodation')
    ward_id = fields.Many2one('res.country.ward', string='Ward')
    district_id = fields.Many2one('res.country.district', string='District')
    state_id = fields.Many2one('res.country.state', string='State')
    accommodation_street = fields.Char(compute='_compute_accommodation_street',
                                       string='Address')
    total_customer = fields.Integer()
    old_customer = fields.Integer()
    new_customer = fields.Integer()
    in_customer = fields.Integer(string='Staying customer')
    out_customer = fields.Integer(string='Checkout customer')
    partner_id = fields.Many2one('res.partner', string='Partner')
    partner_name = fields.Char(string='Management Unit')

    @api.multi
    def _compute_accommodation_street(self):
        for report in self:
            accommodation_street = \
                u'{}-{}-{}'.format(
                    report.ward_id.name or '',
                    report.district_id.name or '',
                    report.state_id.name or '',
                )
            accommodation_street = accommodation_street.replace('--', '-')
            if accommodation_street[0] == '-':
                accommodation_street = accommodation_street[1:]
            report.accommodation_street = accommodation_street

    def init(self):
        self._cr.execute("""
        CREATE OR REPLACE FUNCTION calculate_old_customer(acc_id integer)
         RETURNS integer AS
         $BODY$
         DECLARE old_customer integer;
         BEGIN
            select count(id) into old_customer
            from bms_guest_stay
            where accommodation_id = acc_id
            and CURRENT_DATE != from_date::date;
            RETURN old_customer;
         END;
         $BODY$
         LANGUAGE plpgsql VOLATILE
         COST 100;
        """)

        self._cr.execute("""
         CREATE OR REPLACE FUNCTION calculate_new_customer(acc_id integer)
         RETURNS integer AS
         $BODY$
         DECLARE new_customer integer;
         BEGIN
            select count(id) into new_customer
            from bms_guest_stay
            where accommodation_id = acc_id
            and CURRENT_DATE = from_date::date;
            RETURN new_customer;
         END;
         $BODY$
         LANGUAGE plpgsql VOLATILE
         COST 100;
        """)

        self._cr.execute("""
         CREATE OR REPLACE FUNCTION calculate_in_customer(acc_id integer)
         RETURNS integer AS
         $BODY$
         DECLARE in_customer integer;
         BEGIN
            select count(id) into in_customer
            from bms_guest_stay
            where accommodation_id = acc_id
            and day_number = 0;
            RETURN in_customer;
         END;
         $BODY$
         LANGUAGE plpgsql VOLATILE
         COST 100;
        """)

        self._cr.execute("""
        CREATE OR REPLACE FUNCTION calculate_out_customer(acc_id integer)
        RETURNS integer AS
        $BODY$
        DECLARE out_customer integer;
        BEGIN
            select count(id) into out_customer
            from bms_guest_stay
            where accommodation_id = acc_id
            and day_number = -1;
            RETURN out_customer;
        END;
        $BODY$
        LANGUAGE plpgsql VOLATILE
        COST 100;
        """)

        self._cr.execute("""
        CREATE OR REPLACE FUNCTION calculate_total_customer(acc_id integer)
        RETURNS integer AS
        $BODY$
        DECLARE total_customer integer;
        BEGIN
            select count(id) into total_customer
            from bms_guest_stay
            where accommodation_id = acc_id;
            RETURN total_customer;
        END;
        $BODY$
        LANGUAGE plpgsql VOLATILE
        COST 100;
        """)

        tools.drop_view_if_exists(self._cr, self._table)
        self._cr.execute("""
                    CREATE or REPLACE view report_guest_inday as (
        SELECT
        a.id as id
        ,a.name as name
        ,a.street as street
        ,a.state_id as state_id
        ,a.district_id as district_id
        ,a.ward_id as ward_id
        ,a.partner_id as partner_id
        ,rp1.name as partner_name
        ,calculate_old_customer(a.id)::integer as old_customer
        ,calculate_new_customer(a.id)::integer as new_customer
        ,calculate_in_customer(a.id)::integer as in_customer
        ,calculate_out_customer(a.id)::integer as out_customer
        ,calculate_total_customer(a.id)::integer as total_customer
        FROM bms_accommodation as a
        left join res_partner as rp1 on rp1.id = a.partner_id
        ORDER BY a.id desc
                    )
                """)


class ReportGuestIndayXlsx(ReportXlsx):
    def generate_xlsx_report(self, workbook, data, wizard):
        sheet = workbook.add_worksheet('Khachtrongngay')

        header_bold = workbook.add_format({'bold': True, 'valign': 'vcenter'})
        header_bold.set_text_wrap()
        header_bold.set_align('center')
        header_bold.set_font_name('Times New Roman')
        header_bold.set_bottom()
        header_bold.set_top()
        header_bold.set_left()
        header_bold.set_right()
        header_bold.set_font_size(17)
        header_bold.set_fg_color('#bcdb0f')
        header_bold.set_font_color('blue')

        bold = workbook.add_format({'bold': True, 'valign': 'vcenter'})
        bold.set_text_wrap()
        bold.set_align('center')
        bold.set_font_name('Times New Roman')
        bold.set_font_size(11)

        border_bold = workbook.add_format({'bold': True, 'valign': 'vcenter'})
        border_bold.set_text_wrap()
        border_bold.set_align('center')
        border_bold.set_font_name('Times New Roman')
        border_bold.set_bottom()
        border_bold.set_top()
        border_bold.set_left()
        border_bold.set_right()
        border_bold.set_fg_color('#efad07')
        border_bold.set_font_color('blue')

        red_bold_left = workbook.add_format(
            {'bold': True, 'valign': 'vcenter'})
        red_bold_left.set_text_wrap()
        red_bold_left.set_align('left')
        red_bold_left.set_font_name('Times New Roman')
        red_bold_left.set_bottom()
        red_bold_left.set_top()
        red_bold_left.set_left()
        red_bold_left.set_right()
        red_bold_left.set_fg_color('pink')

        orange_bold_left = workbook.add_format(
            {'bold': True, 'valign': 'vcenter'})
        orange_bold_left.set_text_wrap()
        orange_bold_left.set_align('left')
        orange_bold_left.set_font_name('Times New Roman')
        orange_bold_left.set_bottom()
        orange_bold_left.set_top()
        orange_bold_left.set_left()
        orange_bold_left.set_right()
        orange_bold_left.set_fg_color('yellow')

        normal = workbook.add_format({'valign': 'vcenter'})
        normal.set_text_wrap()
        normal.set_align('center')
        normal.set_font_name('Times New Roman')

        normal_normal = workbook.add_format({'valign': 'vcenter'})
        normal_normal.set_font_name('Times New Roman')
        normal_normal.set_text_wrap()
        normal_normal.set_top()
        normal_normal.set_bottom()
        normal_normal.set_left()
        normal_normal.set_right()

        normal_normal_note = workbook.add_format({'valign': 'vcenter'})
        normal_normal_note.set_font_name('Times New Roman')
        normal_normal_note.set_font_size(9)

        bold_1 = workbook.add_format({'bold': True, 'valign': 'vcenter'})
        bold_1.set_font_name('Times New Roman')
        bold_1.set_font_size(9)

        bold_2 = workbook.add_format({'bold': True, 'valign': 'vcenter'})
        bold_2.set_font_name('Times New Roman')
        bold_2.set_font_size(9)
        bold_2.set_font_color('blue')

        sheet.set_default_row(25)
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 25)
        sheet.set_column('C:C', 40)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 22)
        sheet.set_column('G:G', 22)
        sheet.set_column('H:H', 22)

        datas = wizard.get_datas()
        day = datetime.utcnow().day
        month = datetime.utcnow().month
        year = datetime.utcnow().year
        row_pos = 1
        sheet.merge_range('A{}:C{}'.format(row_pos, row_pos),
                          u'BỘ CÔNG AN', bold)
        sheet.merge_range('D{}:H{}'.format(row_pos, row_pos),
                          u'CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM', bold)

        row_pos += 1
        sheet.merge_range('A{}:C{}'.format(row_pos, row_pos),
                          u'TAI KHOAN THU NGHIEM', bold)
        sheet.merge_range('D{}:H{}'.format(row_pos, row_pos),
                          u'Độc lập - Tự do - Hạnh phúc', bold)

        row_pos += 1
        date = u'Ngày {} tháng {} năm {}'.format(day, month, year)
        sheet.merge_range('F{}:H{}'.format(row_pos, row_pos), date, normal)

        row_pos += 2
        sheet.merge_range('A{}:H{}'.format(row_pos, row_pos),
                          u'BÁO CÁO KHÁCH TRONG NGÀY',
                          header_bold)

        row_pos += 1
        sheet.write(row_pos, 0, u'STT', border_bold)
        sheet.write(row_pos, 1, u'TÊN CƠ SỞ LƯU TRÚ', border_bold)
        sheet.write(row_pos, 2, u'ĐỊA CHỈ', border_bold)
        sheet.write(row_pos, 3, u'KHÁCH CŨ', border_bold)
        sheet.write(row_pos, 4, u'KHÁCH MỚI', border_bold)
        sheet.write(row_pos, 5, u'KHÁCH ĐÃ TRẢ PHÒNG', border_bold)
        sheet.write(row_pos, 6, u'KHÁCH ĐANG Ở', border_bold)
        sheet.write(row_pos, 7, u'TỔNG SỐ KHÁCH', border_bold)

        row_pos += 1
        for i in range(0, 8):
            sheet.write(row_pos, i, '({})'.format(i + 1), border_bold)

        row_pos += 1
        sequence = 1
        for data in datas:
            sheet.write(row_pos, 0, sequence or '', normal_normal)
            sheet.write(row_pos, 1, data.name or '', normal_normal)
            sheet.write(row_pos, 2, data.accommodation_street or '',
                        normal_normal)
            sheet.write(row_pos, 3, data.old_customer or 0, normal_normal)
            sheet.write(row_pos, 4, data.new_customer or 0, normal_normal)
            sheet.write(row_pos, 5, data.in_customer or 0, normal_normal)
            sheet.write(row_pos, 6, data.out_customer or 0, normal_normal)
            sheet.write(row_pos, 7, data.total_customer or 0, normal_normal)

            row_pos += 1
            sequence += 1


ReportGuestIndayXlsx('report.bms_guest_stay_report.guest_inday.xlsx',
                     'wizard.report.guest.inday')
