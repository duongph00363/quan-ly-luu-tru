# -*- coding: utf-8 -*-
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
import datetime
from odoo import fields, models, _, tools, api


class ReportForeignGuestStayDeclaration(models.Model):
    _name = 'report.foreign.guest.stay.declaration'
    _auto = False

    name = fields.Char()
    gender = fields.Selection([('male', 'Male'),
                               ('female', 'Female'),
                               ('undefine', 'Un define')])
    birth_day = fields.Date()
    nation = fields.Char()
    passport_type = fields.Selection(
        [('ordinary_passport', 'OR: Ordinary Passport'),
         ('official_passport', 'OF: Official Passport'),
         ('diplomatic_passport', 'DP: Diplomatic Passport')],
        default='ordinary_passport')
    identification_type = fields.Selection([('identity_card',
                                             _('Identity card')),
                                            ('passport', _('Passport')),
                                            ('driving_license',
                                             _('Driving license'))])
    identification_number = fields.Char()
    duration = fields.Date()
    date_of_issue = fields.Date()
    place_of_issue = fields.Many2one('res.partner')

    entry_date = fields.Date()
    border_gate = fields.Many2one('bms.guest.border.gate')
    country_id = fields.Many2one('res.country', string='Country')
    state_id = fields.Many2one('res.country.state',
                               string='State',
                               domain=lambda self:
                               [('country_id', '=',
                                 self.env.ref('base.vn').id)])
    district_id = fields.Many2one('res.country.district',
                                  string='District')
    ward_id = fields.Many2one('res.country.ward',
                              string='Ward')
    accommodation_id = fields.Many2one('bms.accommodation',
                                       string='Accommodation')
    partner_id = fields.Many2one('res.partner',
                                 string='Management unit')
    sign = fields.Char()
    from_date = fields.Datetime()
    from_date_date = fields.Date()
    to_date = fields.Datetime()
    to_date_date = fields.Date()
    reason = fields.Many2one('bms.guest.reason')

    accommodation_street = fields.Char(compute='_compute_accommodation_street')

    @api.multi
    def _compute_accommodation_street(self):
        for report in self:
            report.accommodation_street = \
                u'{}-{}-{}'.format(
                    report.accommodation_id.ward_id.name,
                    report.accommodation_id.district_id.name,
                    report.accommodation_id.state_id.name,
                )

    def init(self):
        tools.drop_view_if_exists(self._cr, self._table)
        vn_country_id = self.env.ref('base.vn').id
        self._cr.execute(
            """
                CREATE or REPLACE view
                report_foreign_guest_stay_declaration as (
                select
                  g.id as id,
                  g.name as name,
                  g.gender,
                  g.birth_day,
                  g.nation,
                  g.passport_type,
                  g.identification_number,
                  g.identification_type,
                  g.duration,
                  g.date_of_issue,
                  g.place_of_issue,
                  g.entry_date,
                  g.border_gate,
                  g.country_id,
                  p.state_id,
                  p.district_id,
                  p.ward_id,
                  g.accommodation_id,
                  a.partner_id,
                  g.sign,
                  g.from_date,
                  g.from_date::date as from_date_date,
                  g.to_date,
                  g.to_date::date as to_date_date,
                  g.reason

                from bms_guest_stay as g
                  left join bms_accommodation as a on a.id = g.accommodation_id
                  left join res_partner as p on p.id = a.partner_id
                where g.country_id != %s
                  and g.overseas_vietnamese is false
                )
            """, [vn_country_id]
        )


class ReportForeignGuestStayDeclarationXlsx(ReportXlsx):
    def increment_char(self, src_char, add=1):
        return chr(ord(src_char) + add)

    def format_date(self, src_date):
        if src_date:
            src_date = datetime.datetime.strptime(src_date, '%Y-%m-%d')
            return src_date.strftime('%d-%m-%Y')
        return src_date

    def generate_xlsx_report(self, workbook, data, wizards):
        template = wizards.template
        report_name = _('Foreign guest stay declaration NA17') + ' ' + template

        sheet = workbook.add_worksheet(report_name[:31])

        header_bold = workbook.add_format({'bold': True})
        header_bold.set_text_wrap()
        header_bold.set_align('center')
        header_bold.set_font_name('Times New Roman')
        header_bold.set_bottom()
        header_bold.set_top()
        header_bold.set_left()
        header_bold.set_right()
        header_bold.set_font_size(19)
        header_bold.set_fg_color('green')

        header2_bold = workbook.add_format({'bold': True})
        header2_bold.set_text_wrap()
        header2_bold.set_align('center')
        header2_bold.set_font_name('Times New Roman')
        header2_bold.set_bottom()
        header2_bold.set_top()
        header2_bold.set_left()
        header2_bold.set_right()
        header2_bold.set_font_size(15)
        header2_bold.set_fg_color('green')

        italic = workbook.add_format({'italic': True})
        italic.set_text_wrap()
        italic.set_align('center')
        italic.set_font_name('Times New Roman')
        italic.set_font_size(11)

        bold = workbook.add_format({'bold': True})
        bold.set_text_wrap()
        bold.set_align('center')
        bold.set_font_name('Times New Roman')
        bold.set_font_size(11)

        border_bold = workbook.add_format({'bold': True})
        border_bold.set_text_wrap()
        border_bold.set_align('center')
        border_bold.set_font_name('Times New Roman')
        border_bold.set_bottom()
        border_bold.set_top()
        border_bold.set_left()
        border_bold.set_right()
        border_bold.set_fg_color('gray')

        red_bold_left = workbook.add_format({'bold': True})
        red_bold_left.set_text_wrap()
        red_bold_left.set_align('left')
        red_bold_left.set_font_name('Times New Roman')
        red_bold_left.set_bottom()
        red_bold_left.set_top()
        red_bold_left.set_left()
        red_bold_left.set_right()
        red_bold_left.set_fg_color('pink')

        orange_bold_left = workbook.add_format({'bold': True})
        orange_bold_left.set_text_wrap()
        orange_bold_left.set_align('left')
        orange_bold_left.set_font_name('Times New Roman')
        orange_bold_left.set_bottom()
        orange_bold_left.set_top()
        orange_bold_left.set_left()
        orange_bold_left.set_right()
        orange_bold_left.set_fg_color('yellow')

        normal_left = workbook.add_format({})
        normal_left.set_text_wrap()
        normal_left.set_align('left')
        normal_left.set_font_name('Times New Roman')
        normal_left.set_bottom()
        normal_left.set_top()
        normal_left.set_left()
        normal_left.set_right()

        normal_center = workbook.add_format({})
        normal_center.set_text_wrap()
        normal_center.set_align('center')
        normal_center.set_font_name('Times New Roman')
        normal_center.set_bottom()
        normal_center.set_top()
        normal_center.set_left()
        normal_center.set_right()

        if template in ('2', '3'):
            sheet.set_column('A:A', 5)
            sheet.set_column('B:B', 26)
            sheet.set_column('C:C', 9)
            sheet.set_column('D:D', 9)
            sheet.set_column('E:E', 12)
            sheet.set_column('F:F', 16)
            sheet.set_column('G:G', 26)
            sheet.set_column('H:H', 28)
            sheet.set_column('I:I', 18)
            sheet.set_column('J:J', 18)
            sheet.set_column('K:K', 18)
            sheet.set_column('L:L', 18)
        elif template == '1':
            sheet.set_column('A:A', 5)
            sheet.set_column('B:B', 26)
            sheet.set_column('C:C', 9)
            sheet.set_column('D:D', 9)
            sheet.set_column('E:E', 12)
            sheet.set_column('F:F', 16)
            sheet.set_column('G:G', 26)
            sheet.set_column('H:H', 10)
            sheet.set_column('I:I', 18)
            sheet.set_column('J:J', 18)
            sheet.set_column('K:K', 18)
            sheet.set_column('L:L', 18)
            sheet.set_column('M:M', 22)
            sheet.set_column('N:N', 18)
            sheet.set_column('O:O', 18)

        report_s, accommodation_dict = wizards.get_data()
        identification_type_tr_selection = \
            wizards.get_tr_selection('identification_type')
        passport_type_tr_selection = \
            wizards.get_tr_selection('passport_type')

        row_pos = 1

        sheet.merge_range('E{}:G{}'.format(row_pos, row_pos),
                          u'Mẫu NA17', bold)
        row_pos += 1

        sheet.merge_range('E{}:G{}'.format(row_pos, row_pos),
                          u'Ban hành theo TT số 04/2015/TT-BCA', bold)
        row_pos += 1

        sheet.merge_range('E{}:G{}'.format(row_pos, row_pos),
                          u'Ngày 5 tháng 1 năm 2015',
                          bold)
        row_pos += 1

        sheet.merge_range('B{}:F{}'.format(row_pos, row_pos),
                          u'CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM', bold)
        row_pos += 1

        sheet.merge_range('B{}:F{}'.format(row_pos, row_pos),
                          u'Độc lập - Tự do - Hạnh phúc', bold)
        sheet.write(row_pos - 1, 6,
                    u'Ngày {} tháng {} năm {}'.format(
                        datetime.datetime.now().day,
                        datetime.datetime.now().month,
                        datetime.datetime.now().year,
                    ),
                    italic)
        row_pos += 1
        sheet.set_row(row_pos - 1, 30)
        sheet.merge_range('B{}:F{}'.format(row_pos, row_pos),
                          u'PHIẾU KHAI BÁO TẠM TRÚ CHO NGƯỜI NƯỚC NGOÀI',
                          header_bold)
        row_pos += 1
        sheet.set_row(row_pos - 1, 18)
        sheet.merge_range(
            'B{}:F{}'.format(row_pos, row_pos),
            u'(từ ngày {} đến ngày {})'.format(
                self.format_date(wizards.from_date),
                self.format_date(wizards.to_date or '')),
            header2_bold)
        row_pos += 2

        th_label_dict = {
            '3': [u'STT',
                  u'HỌ TÊN',
                  (
                      {'position': 'C{}:D{}',
                       'value': u'GIỚI TÍNH'},
                      ({'position': 2, 'value': u'NAM'},
                       {'position': 3, 'value': u'NỮ'}
                       )
                  ),
                  u'SINH NGÀY',
                  u'QUỐC TỊCH',
                  u'LOẠI / SỐ HỘ CHIẾU',
                  u'LOẠI / THỜI HẠN / SỐ / NGÀY CẤP / CƠ QUAN CẤP THỊ THỰC',
                  u'NGÀY / CỬA KHẨU NHẬP CẢNH',
                  u'MỤC ĐÍCH NHẬP CẢNH',
                  u'TẠM TRÚ (từ ngày...đến ngày…)'],
            '2': [u'STT',
                  u'HỌ TÊN',
                  (
                      {'position': 'C{}:D{}',
                       'value': u'GIỚI TÍNH'},
                      ({'position': 2, 'value': u'NAM'},
                       {'position': 3, 'value': u'NỮ'}
                       )
                  ),
                  u'SINH NGÀY',
                  u'QUỐC TỊCH',
                  u'LOẠI / SỐ HỘ CHIẾU',
                  u'LOẠI / THỜI HẠN / SỐ / NGÀY CẤP / CƠ QUAN CẤP THỊ THỰC',
                  u'NGÀY / CỬA KHẨU NHẬP CẢNH',
                  u'MỤC ĐÍCH NHẬP CẢNH',
                  u'TẠM TRÚ (từ ngày...đến ngày…)',
                  u'TÊN CƠ SỞ LƯU TRÚ/ĐỊA CHỈ'],
            '1': [u'STT',
                  u'HỌ TÊN',
                  (
                      {'position': 'C{}:D{}',
                       'value': u'GIỚI TÍNH'},
                      (
                          {'position': 2, 'value': u'NAM'},
                          {'position': 3, 'value': u'NỮ'}
                      )
                  ),
                  u'SINH NGÀY',
                  u'QUỐC TỊCH',
                  u'LOẠI / SỐ HỘ CHIẾU',
                  (
                      {'position': 'H{}:L{}',
                       'value': u'THỊ THỰC / GIẤY TỜ KHÁC'},
                      (
                          {'position': 7,
                           'value': u'SỐ'},
                          {'position': 8,
                           'value': u'LOẠI GIẤY TỜ'},
                          {'position': 9,
                           'value': u'KÝ HIỆU'},
                          {'position': 10,
                           'value': u'THỜI HẠN'},
                          {'position': 11,
                           'value': u'CƠ QUAN CẤP'},
                      )
                  ),
                  u'NGÀY / CỬA KHẨU NHẬP CẢNH',
                  u'MỤC ĐÍCH NHẬP CẢNH',
                  u'TẠM TRÚ (từ ngày...đến ngày…)'],
        }

        th_label = th_label_dict[template]

        col_pos = 'A'
        for label in th_label:
            if type(label) == tuple:
                sheet.merge_range(
                    label[0]['position'].format(row_pos, row_pos),
                    label[0]['value'], border_bold)
                for l in label[1]:
                    sheet.write(row_pos, l['position'],
                                l['value'], border_bold)
                col_pos = self.increment_char(col_pos, len(label[1]))
                continue
            sheet.merge_range(
                '{}{}:{}{}'.format(col_pos, row_pos, col_pos, row_pos + 1),
                label, border_bold)
            col_pos = self.increment_char(col_pos)
        row_pos += 1

        index_dict = {
            '1': 15,
            '2': 12,
            '3': 11,
        }

        for col_num in range(0, index_dict[template]):
            sheet.write(row_pos, col_num,
                        '({})'.format(col_num + 1),
                        border_bold)
        row_pos += 1
        sheet.freeze_panes(row_pos, 11)

        style_dict = {
            0: normal_center,
            2: normal_center,
            3: normal_center,
            4: normal_center,
        }

        count = 0
        for line in report_s:
            count += 1
            male = ''
            if line['gender'] == 'male':
                male = 'x'

            female = ''
            if line['gender'] == 'female':
                female = 'x'

            passport_type = passport_type_tr_selection.get(
                line['passport_type'], '') or ''

            birth_day = self.format_date(line['birth_day'] or '')
            country_id = line['country_id'][1]
            passport_type_identification_number = u'{} / {}'.format(
                passport_type,
                line['identification_number']
            )
            i_type_duration_i_number_date_place_of_issue = \
                u'{} / {} / {} / {} / {}'.format(
                    identification_type_tr_selection.get(
                        line['identification_type']),
                    self.format_date(line['duration']) or '',
                    line['identification_number'],
                    self.format_date(line['date_of_issue']) or '',
                    line['place_of_issue'] and line['place_of_issue'][1] or ''
                )
            entry_date_border_gate = u'{} / {}'.format(
                self.format_date(line['entry_date'] or ''),
                line['border_gate'] and
                line['border_gate'][1] or ''
            )
            reason = line['reason'] and line['reason'][1] or ''
            from_date_to_date = '{} - {}'.format(
                self.format_date(line['from_date_date']),
                self.format_date(line['to_date_date']) or '',
            )
            accommodation_street = u'{}{}'.format(
                line['accommodation_id'][1],
                accommodation_dict.get(line['accommodation_id'][0], '')
            )
            identification_number = line['identification_number']
            identification_type = identification_type_tr_selection.get(
                line['identification_type'], '')
            sign = line['sign'] or ''
            duration = \
                line['duration'] and self.format_date(line['duration']) or ''
            place_of_issue = \
                line['place_of_issue'] and line['place_of_issue'][1] or ''

            line_values_dict = {'3': [
                count,
                line['name'],
                male,
                female,
                birth_day,
                country_id,
                passport_type_identification_number,
                i_type_duration_i_number_date_place_of_issue,
                entry_date_border_gate,
                reason,
                from_date_to_date
            ],
                '2': [
                    count,
                    line['name'],
                    male,
                    female,
                    birth_day,
                    country_id,
                    passport_type_identification_number,
                    i_type_duration_i_number_date_place_of_issue,
                    entry_date_border_gate,
                    reason,
                    from_date_to_date,
                    accommodation_street
                ],
                '1': [
                    count,
                    line['name'],
                    male,
                    female,
                    birth_day,
                    country_id,
                    passport_type_identification_number,
                    identification_number,
                    identification_type,
                    sign,
                    duration,
                    place_of_issue,
                    entry_date_border_gate,
                    reason,
                    from_date_to_date
                ],
            }
            line_values = line_values_dict[template]

            col_num = 0
            for line_value in line_values:
                style = style_dict.get(col_num, normal_left)
                sheet.write(row_pos, col_num, line_value, style)
                col_num += 1
            sheet.set_column('A:A', 5)
            sheet.set_row(row_pos, 20)
            row_pos += 1


ReportForeignGuestStayDeclarationXlsx(
    'report.bms_guest_stay.foreign_guest_stay_declaration_xls.xlsx',
    'wizard.report.foreign.guest.stay.declaration')
