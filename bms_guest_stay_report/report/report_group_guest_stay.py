# -*- coding: utf-8 -*-
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
import datetime
from odoo import fields, models, _, tools


class ReportGroupGuestStay(models.Model):
    _name = 'report.group.guest.stay'
    _auto = False

    group_id = fields.Many2one('res.partner', 'Group')
    state_id = fields.Many2one('res.country.state',
                               string='State',
                               domain=lambda self:
                               [('country_id', '=',
                                 self.env.ref('base.vn').id)])
    district_id = fields.Many2one('res.country.district',
                                  string='District')
    ward_id = fields.Many2one('res.country.ward',
                              string='Ward')
    accommodation_id = fields.Many2one('bms.accommodation',
                                       string='Accommodation')
    partner_id = fields.Many2one('res.partner',
                                 string='Management unit')
    from_date = fields.Date()
    to_date = fields.Date()

    def init(self):
        group_tag_id = self.env.ref(
            'bms_guest_stay.res_partner_category_group_customer').id
        tools.drop_view_if_exists(self._cr, self._table)
        self._cr.execute(
            """
                CREATE or REPLACE view
                report_group_guest_stay as (
                select
                  g.id as id,
                  g.from_date,
                  g.to_date,
                  r.id as group_id,
                  a.id as accommodation_id,
                  a.partner_id as partner_id,
                  a.state_id,
                  a.district_id,
                  a.ward_id
                from res_partner as r
                  left join res_partner_res_partner_category_rel as rel
                  on r.id = rel.partner_id
                  left join bms_guest_stay as g
                  on g.group_id = r.id and g.type = 'group'
                  left join bms_accommodation as a on a.id = g.accommodation_id
                where rel.category_id = %s
                )
            """, [group_tag_id])


class ReportGroupGuestStayXlsx(ReportXlsx):
    def increment_char(self, src_char, add=1):
        return chr(ord(src_char) + add)

    def format_date(self, src_date):
        if src_date:
            src_date = datetime.datetime.strptime(src_date, '%Y-%m-%d')
            return src_date.strftime('%d-%m-%Y')
        return src_date

    def generate_xlsx_report(self, workbook, data, wizards):
        report_name = _('Group guest stay')
        sheet = workbook.add_worksheet(report_name[:31])
        header_bold = workbook.add_format({'bold': True})
        header_bold.set_text_wrap()
        header_bold.set_align('center')
        header_bold.set_font_name('Times New Roman')
        header_bold.set_bottom()
        header_bold.set_top()
        header_bold.set_left()
        header_bold.set_right()
        header_bold.set_font_size(19)
        header_bold.set_fg_color('green')

        italic = workbook.add_format({'italic': True})
        italic.set_text_wrap()
        italic.set_align('center')
        italic.set_font_name('Times New Roman')
        italic.set_font_size(11)

        bold = workbook.add_format({'bold': True})
        bold.set_text_wrap()
        bold.set_align('center')
        bold.set_font_name('Times New Roman')
        bold.set_font_size(11)

        border_bold = workbook.add_format({'bold': True})
        border_bold.set_text_wrap()
        border_bold.set_align('center')
        border_bold.set_font_name('Times New Roman')
        border_bold.set_bottom()
        border_bold.set_top()
        border_bold.set_left()
        border_bold.set_right()
        border_bold.set_fg_color('gray')

        normal_left = workbook.add_format({})
        normal_left.set_text_wrap()
        normal_left.set_align('left')
        normal_left.set_font_name('Times New Roman')
        normal_left.set_bottom()
        normal_left.set_top()
        normal_left.set_left()
        normal_left.set_right()

        normal_center = workbook.add_format({})
        normal_center.set_text_wrap()
        normal_center.set_align('center')
        normal_center.set_font_name('Times New Roman')
        normal_center.set_bottom()
        normal_center.set_top()
        normal_center.set_left()
        normal_center.set_right()

        sheet.set_column('A:A', 10)
        sheet.set_column('B:B', 36)
        sheet.set_column('C:C', 36)
        sheet.set_column('D:D', 12)

        report_s, sum = wizards.get_data()

        row_pos = 1
        sheet.merge_range('C{}:D{}'.format(row_pos, row_pos),
                          u'CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM', bold)
        row_pos += 1

        sheet.merge_range('C{}:D{}'.format(row_pos, row_pos),
                          u'Độc lập - Tự do - Hạnh phúc', bold)

        row_pos += 1
        sheet.merge_range(
            'C{}:D{}'.format(row_pos, row_pos),
            u'Ngày {} tháng {} năm {}'.format(
                datetime.datetime.now().day,
                datetime.datetime.now().month,
                datetime.datetime.now().year),
            italic)
        row_pos += 1
        sheet.set_row(row_pos - 1, 30)
        sheet.merge_range(
            'A{}:D{}'.format(row_pos, row_pos),
            u'THỐNG KÊ KHÁCH LƯU TRÚ THEO ĐOÀN NGÀY {}/{}/{}'.format(
                datetime.datetime.now().day,
                datetime.datetime.now().month,
                datetime.datetime.now().year),
            header_bold)
        row_pos += 1
        sheet.set_row(row_pos - 1, 18)

        th_label = [u'STT',
                    u'TÊN ĐOÀN',
                    u'TÊN CƠ SỞ LƯU TRÚ',
                    u'SỐ LƯỢNG']
        col_num = 0
        for th in th_label:
            sheet.write(row_pos, col_num,
                        th,
                        border_bold)
            sheet.write(row_pos + 1, col_num,
                        '({})'.format(col_num + 1),
                        border_bold)
            col_num += 1

        row_pos += 2

        row_num = 0
        for line_value in report_s:
            row_num += 1
            sheet.write(row_pos, 0, row_num, normal_center)
            sheet.write(row_pos, 1, line_value['group_name'], normal_left)
            sheet.write(row_pos, 2,
                        line_value['accommodation_name'], normal_left)
            sheet.write(row_pos, 3, line_value['count'], normal_center)
            row_pos += 1
        sheet.set_column('A:A', 5)
        sheet.set_row(row_pos, 20)

        sheet.write(row_pos, 0,
                    '',
                    border_bold)
        sheet.write(row_pos, 1,
                    u'TỔNG CỘNG:',
                    border_bold)
        sheet.write(row_pos, 2,
                    '',
                    border_bold)
        sheet.write(row_pos, 3,
                    sum,
                    border_bold)
        row_pos += 3

        sheet.write(row_pos, 1,
                    u'NGƯỜI BÁO CÁO',
                    bold)

        sheet.write(row_pos, 2,
                    u'THỦ TRƯỞNG ĐƠN VỊ',
                    bold)


ReportGroupGuestStayXlsx(
    'report.bms_guest_stay.group_guest_stay_xls.xlsx',
    'wizard.report.group.guest.stay')
