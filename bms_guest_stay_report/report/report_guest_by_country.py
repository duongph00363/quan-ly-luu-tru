# -*- coding: utf-8 -*-
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
import datetime
from odoo import fields, models, tools


class ReportGuestbyCountry(models.Model):
    _name = 'report.guest.by.country'
    _auto = False

    name = fields.Char()
    accommodation_id = fields.Many2one('bms.accommodation',
                                       string='Accommodation')
    partner_id = fields.Many2one('res.partner',
                                 string='Management unit')
    state_id = fields.Many2one('res.country.state',
                               string='State')
    district_id = fields.Many2one('res.country.district',
                                  string='District')
    ward_id = fields.Many2one('res.country.ward',
                              string='Ward')
    from_date = fields.Date()
    to_date = fields.Date()
    gender = fields.Selection([('male', 'Male'),
                               ('female', 'Female'),
                               ('undefine', 'Un define')],
                              default='male')
    is_entry = fields.Boolean()
    overseas_vietnamese = fields.Boolean()
    guest_country_id = fields.Many2one('res.country', string='Guest country')

    def init(self):
        tools.drop_view_if_exists(self._cr, self._table)
        vn_country_id = self.env.ref('base.vn').id
        self._cr.execute(
            """CREATE or REPLACE view report_guest_by_country as (
                select
                  g.id as id,
                  g.name as name,
                  g.accommodation_id as accommodation_id,
                  a.partner_id as partner_id,
                  p.state_id as state_id,
                  p.district_id as district_id,
                  p.ward_id as ward_id,
                  g.from_date,
                  g.to_date,
                  g.gender,
                  g.overseas_vietnamese,
                  g.country_id as guest_country_id,
                  case when g.country_id = %s then false
                   else true end as is_entry
                from bms_guest_stay as g
                    left join bms_accommodation
                    as a on g.accommodation_id = a.id
                    left join res_partner as p on p.id = a.partner_id
                )
            """, [vn_country_id]
        )


class ReportGuestBycountryXlsx(ReportXlsx):
    def generate_xlsx_report(self, workbook, data, wizards):
        report_name = str(wizards.id)
        sheet = workbook.add_worksheet(report_name[:31])

        header_bold = workbook.add_format({'bold': True})
        header_bold.set_text_wrap()
        header_bold.set_align('center')
        header_bold.set_font_name('Times New Roman')
        header_bold.set_bottom()
        header_bold.set_top()
        header_bold.set_left()
        header_bold.set_right()
        header_bold.set_font_size(17)
        header_bold.set_fg_color('green')

        bold = workbook.add_format({'bold': True})
        bold.set_text_wrap()
        bold.set_align('center')
        bold.set_font_name('Times New Roman')
        bold.set_font_size(11)

        border_bold = workbook.add_format({'bold': True})
        border_bold.set_text_wrap()
        border_bold.set_align('center')
        border_bold.set_font_name('Times New Roman')
        border_bold.set_bottom()
        border_bold.set_top()
        border_bold.set_left()
        border_bold.set_right()
        border_bold.set_fg_color('gray')

        red_bold_left = workbook.add_format({'bold': True})
        red_bold_left.set_text_wrap()
        red_bold_left.set_align('left')
        red_bold_left.set_font_name('Times New Roman')
        red_bold_left.set_bottom()
        red_bold_left.set_top()
        red_bold_left.set_left()
        red_bold_left.set_right()
        red_bold_left.set_fg_color('pink')

        orange_bold_left = workbook.add_format({'bold': True})
        orange_bold_left.set_text_wrap()
        orange_bold_left.set_align('left')
        orange_bold_left.set_font_name('Times New Roman')
        orange_bold_left.set_bottom()
        orange_bold_left.set_top()
        orange_bold_left.set_left()
        orange_bold_left.set_right()
        orange_bold_left.set_fg_color('yellow')

        normal = workbook.add_format({})
        normal.set_text_wrap()
        normal.set_align('left')
        normal.set_font_name('Times New Roman')
        normal.set_bottom()
        normal.set_top()
        normal.set_left()
        normal.set_right()

        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 28)
        sheet.set_column('C:C', 13)
        sheet.set_column('D:D', 13)
        sheet.set_column('E:E', 13)
        sheet.set_column('F:F', 16)
        sheet.set_column('G:G', 16)
        sheet.set_column('H:H', 16)

        count_dict, female_count_dict, \
            male_count_dict, entry_count_dict, vn_count_dict, \
            overseas_vn_count_dict, country_dict = wizards.get_data()

        row_pos = 1

        sheet.merge_range('A{}:B{}'.format(row_pos, row_pos), u'BỘ CÔNG AN',
                          bold)
        sheet.merge_range('F{}:H{}'.format(row_pos, row_pos),
                          u'CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM', bold)
        row_pos += 1

        sheet.merge_range('F{}:H{}'.format(row_pos, row_pos),
                          u'Độc lập - Tự do - Hạnh phúc', bold)
        row_pos += 2

        header_label = \
            u'TÌNH HÌNH LƯU TRÚ THEO QUỐC TỊCH NGÀY {} đến {}'.format(
                datetime.datetime.strptime(
                    wizards.from_date,
                    '%Y-%m-%d').strftime('%d-%m-%Y'),
                datetime.datetime.strptime(
                    wizards.to_date,
                    '%Y-%m-%d').strftime('%d-%m-%Y')
            )
        sheet.merge_range('A{}:H{}'.format(row_pos, row_pos + 1), header_label,
                          header_bold)
        row_pos += 3

        sheet.merge_range('A{}:A{}'.format(row_pos, row_pos + 1),
                          u'STT', border_bold)
        sheet.merge_range('B{}:B{}'.format(row_pos, row_pos + 1),
                          u'QUỐC TỊCH', border_bold)
        sheet.merge_range('C{}:C{}'.format(row_pos, row_pos + 1),
                          u'TỔNG SỐ LƯỢT', border_bold)
        sheet.merge_range('D{}:D{}'.format(row_pos, row_pos + 1),
                          u'NAM', border_bold)
        sheet.merge_range('E{}:E{}'.format(row_pos, row_pos + 1),
                          u'NỮ', border_bold)
        sheet.merge_range('F{}:H{}'.format(row_pos, row_pos),
                          u'TRONG ĐÓ', border_bold)

        sheet.write(row_pos, 5,
                    u'NGƯỜI NƯỚC NGOÀI', border_bold)
        sheet.write(row_pos, 6,
                    u'NGƯỜI VN ĐỊNH CƯ Ở NƯỚC NGOÀI',
                    border_bold)
        sheet.write(row_pos, 7,
                    u'NGƯỜI VIỆT NAM',
                    border_bold)
        sheet.set_row(row_pos, 28)

        row_pos += 1
        for i in range(0, 8):
            sheet.write(row_pos, i, '({})'.format(i+1), border_bold)

        row_pos += 1
        count_index = 0
        for country_id in country_dict.keys():
            count_index += 1
            country_name = country_dict[country_id]
            sheet.write(row_pos, 0, count_index, normal)
            sheet.write(row_pos, 1, country_name, normal)

            col = 1
            for count_item_dict in [count_dict,
                                    male_count_dict,
                                    female_count_dict,
                                    entry_count_dict,
                                    overseas_vn_count_dict,
                                    vn_count_dict]:
                col += 1
                count_item = count_item_dict.get(country_id, 0)
                sheet.write(row_pos, col, count_item, normal)
            row_pos += 1

        sheet.write(row_pos, 1, u'TỔNG', border_bold)
        col = 1
        for count_item_dict in [count_dict,
                                male_count_dict,
                                female_count_dict,
                                entry_count_dict,
                                overseas_vn_count_dict,
                                vn_count_dict]:
            col += 1
            count_item = sum(count_item_dict.values())
            sheet.write(row_pos, col, count_item, border_bold)


ReportGuestBycountryXlsx('report.bms_guest_stay.guest_by_country_xls.xlsx',
                         'wizard.report.guest.by.country')
