# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class WizardReportGuestInday(models.TransientModel):
    _name = 'wizard.report.guest.inday'

    def _state_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return self.env.user.state_id.id
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.state_id.id
        return False

    def _district_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.district_id.id
        return False

    def _ward_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.ward_id.id
        return False

    def _management_unit_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.partner_id.id
        return False

    def _is_super_manager_default(self):
        res = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        return res

    state_id = fields.Many2one('res.country.state', string='State',
                               required=True, default=_state_id_default)
    district_id = fields.Many2one('res.country.district', string='District',
                                  default=_district_id_default)
    ward_id = fields.Many2one('res.country.ward', string='Ward',
                              default=_ward_id_default)
    partner_id = fields.Many2one('res.partner', string='Management unit',
                                 default=_management_unit_id_default)

    is_super_manager = fields.Boolean(compute='_compute_is_super_manager',
                                      default=_is_super_manager_default)

    @api.model
    def _compute_is_super_manager(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        self.is_super_manager = is_super_manager

    @api.multi
    def management_unit_onchange(
            self, state_id, district_id, ward_id, context={}):

        res = {
            'value': {}
        }
        if state_id:
            res['domain'] = {
                'partner_id': [('state_id', '=', state_id),
                               ('is_management_unit', '=', True)]
            }

        if district_id:
            res['domain'] = {
                'partner_id':
                    [('district_id', '=', district_id),
                     ('is_management_unit', '=', True)]
            }

        if ward_id:
            res['domain'] = {
                'partner_id': [('ward_id', '=', ward_id),
                               ('is_management_unit', '=', True)]
            }

        if context.get('change_state', False):
            res['value']['district_id'] = False
            res['value']['ward_id'] = False
            res['value']['partner_id'] = False

        if context.get('change_district', False):
            res['value']['ward_id'] = False
            res['value']['partner_id'] = False

        if context.get('change_ward', False):
            res['value']['partner_id'] = False

        return res

    @api.multi
    def get_domain(self):
        domain = []

        state_id = self.state_id.id
        domain.append(('state_id', '=', state_id))

        district_id = self.district_id.id
        if district_id:
            domain.append(('district_id', '=', district_id))

        ward_id = self.ward_id.id
        if ward_id:
            domain.append(('ward_id', '=', ward_id))

        partner_id = self.partner_id.id
        if partner_id:
            domain.append(('partner_id', '=', partner_id))

        return domain

    @api.multi
    def get_datas(self):
        domain = self.get_domain()
        datas = self.env['report.guest.inday'].search(domain)
        return datas

    @api.multi
    def view_report(self):
        domain = self.get_domain()
        action = self.env.ref(
            'bms_guest_stay_report.report_guest_inday_action')
        action_view = action.read()[0]
        action_view['domain'] = domain
        return action_view

    @api.multi
    def print_report(self):
        datas = {'ids': self.ids}
        datas['model'] = 'wizard.report.guest.inday'
        datas['data'] = self.read()[0]
        for field in datas['data'].keys():
            if isinstance(datas['data'][field], tuple):
                datas['data'][field] = datas['data'][field][0]

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'bms_guest_stay_report.guest_inday.xlsx',
            'datas': datas,
            'name': _('Guest In Day Report')
        }
