# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
import datetime
from dateutil.relativedelta import relativedelta
import pytz
from datetime import timedelta
from odoo.exceptions import UserError


class WizardReportGuestByDay(models.TransientModel):
    _name = 'wizard.report.notebook.of.stay'
    _description = 'Wizard report notebook of stay'
    _rec_name = 'id'
    _report_model_name = 'bms.guest.stay'

    def default_from_date(self):
        date_now = datetime.datetime.now()
        date_now = date_now.strftime('%Y-%m') + '-01'
        return date_now

    def default_to_date(self):
        date_now = datetime.datetime.now() + relativedelta(months=1)
        date_now = date_now.strftime('%Y-%m') + '-01'
        date_now = datetime.datetime.strptime(date_now, '%Y-%m-%d')
        date_now = date_now + relativedelta(days=-1)
        date_now = date_now.strftime('%Y-%m-%d')
        return date_now

    @api.multi
    def change_utc_to_local_datetime(self, souce_date):
        user_tz = self.env.user.tz or str(pytz.utc)
        tz_now = datetime.datetime.now(pytz.timezone(user_tz))
        difference = tz_now.utcoffset().total_seconds() / 60 / 60
        difference = int(difference)
        utc_date = datetime.datetime.strptime(souce_date,
                                              '%Y-%m-%d %H:%M:%S')
        utc_date = utc_date + timedelta(hours=-difference)
        return utc_date.strftime('%Y-%m-%d %H:%M:%S')

    def _state_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return self.env.user.state_id.id
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.state_id.id
        return False

    def _district_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.district_id.id
        return False

    def _ward_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.ward_id.id
        return False

    def _management_unit_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.partner_id.id
        return False

    def _is_super_manager_default(self):
        res = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        return res

    state_id = fields.Many2one('res.country.state', string='State',
                               default=_state_id_default)
    district_id = fields.Many2one('res.country.district', string='District',
                                  default=_district_id_default)
    ward_id = fields.Many2one('res.country.ward', string='Ward',
                              default=_ward_id_default)
    management_unit_id = fields.Many2one('res.partner',
                                         'Management unit',
                                         default=_management_unit_id_default)

    from_date = fields.Date(required=True, default=default_from_date)
    to_date = fields.Date(required=True, default=default_to_date)
    national = fields.Selection([('all', 'All'),
                                 ('vietnamese', 'Viet Nam'),
                                 ('foreigner', 'Foreigner')],
                                default='all', required=True,
                                string='Type')

    is_super_manager = fields.Boolean(compute='_compute_is_super_manager',
                                      default=_is_super_manager_default)

    @api.model
    def _compute_is_super_manager(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        self.is_super_manager = is_super_manager

    # def tr(self, source, field):
    #     uid = self.env.uid
    #
    #     user = self.env['res.users'].browse(uid)
    #     lang = user.partner_id.lang
    #     self.env.cr.execute(
    #         '''select value
    #             from ir_translation
    #             where lang=%s and type='selection'
    #             and src=%s and name=%s''',
    #         (lang, source, self._report_model_name + ',' + field))
    #     res_trans = self.env.cr.fetchone()
    #     res = res_trans and res_trans[0] or False
    #     return res
    #
    # @api.model
    # def get_selection_value(self, field_name, value):
    #     model_name = 'bms.guest.stay'
    #
    #     uid = self.env.uid
    #     user = self.env['res.users'].browse(uid)
    #     model_bject = self.env[model_name]. \
    #         with_context(lang=user.partner_id.lang)
    #     field = model_bject._fields[field_name]
    #     selection = field.selection
    #     selection_label_dict = dict((s[0],
    #                                  self.tr(s[1], field_name))
    #                                 for s in selection)
    #     if value not in selection_label_dict.keys():
    #         return ''
    #     return selection_label_dict[value]
    # @api.onchange('state_id')
    # def state_onchange(self):
    #     self.district_id = False
    #
    # @api.onchange('district_id')
    # def district_onchange(self):
    #     self.ward_id = False
    #
    # @api.onchange('ward_id')
    # def ward_onchange(self):
    #     self.management_unit_id = False

    @api.multi
    def management_unit_onchange(
            self, state_id, district_id, ward_id, context={}):

        res = {
            'value': {}
        }
        if state_id:
            res['domain'] = {
                'management_unit_id': [('state_id', '=', state_id),
                                       ('is_management_unit', '=', True)]
            }

        if district_id:
            res['domain'] = {
                'management_unit_id':
                    [('district_id', '=', district_id),
                     ('is_management_unit', '=', True)]
            }

        if ward_id:
            res['domain'] = {
                'management_unit_id': [('ward_id', '=', ward_id),
                                       ('is_management_unit', '=', True)]
            }

        if context.get('change_state', False):
            res['value']['district_id'] = False
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_district', False):
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_ward', False):
            res['value']['management_unit_id'] = False

        return res

    def tr(self, field_name):
        uid = self.env.uid

        user = self.env['res.users'].browse(uid)
        lang = user.partner_id.lang
        name = self._report_model_name + ',' + field_name
        trans = self.env['ir.translation'].search_read([
            ('lang', '=', lang),
            ('type', '=', 'selection'),
            ('name', '=', name)],
            ['source', 'value'])

        res = dict((tran['source'], tran['value']) for tran in trans)
        return res

    @api.model
    def get_tr_selection(self, field_name):
        model_name = 'bms.guest.stay'

        uid = self.env.uid
        user = self.env['res.users'].browse(uid)
        model_bject = self.env[model_name]. \
            with_context(lang=user.partner_id.lang)
        field = model_bject._fields[field_name]
        selection = field.selection
        tr_dict = self.tr(field_name)
        selection_tr_dict = \
            dict((s[0], tr_dict.get(s[1], s[1])) for s in selection)
        return selection_tr_dict

    @api.multi
    def get_datas(self):
        datas = self.env['report.notebook.of.stay'].search(self.get_domain())
        return datas

    @api.multi
    def export_report(self):
        datas = {'ids': self.ids}
        datas['model'] = 'wizard.report.notebook.of.stay'
        datas['form'] = self.read()[0]
        for field in datas['form'].keys():
            if isinstance(datas['form'][field], tuple):
                datas['form'][field] = datas['form'][field][0]
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'bms_guest_stay.notebook_of_stay_xls.xlsx',
            'datas': datas,
            'name': _('Notebook Of Stay')
        }

    # @api.multi
    # def get_domain(self):
    #     domain = []
    #     from_date = self.from_date + ' 00:00:00'
    #     from_date = self.change_utc_to_local_datetime(from_date)
    #     to_date = self.to_date + ' 23:59:59'
    #     to_date = self.change_utc_to_local_datetime(to_date)
    #     state = self.state_id
    #     national = self.national
    #     if state:
    #         domain.append(
    #             ('state_id', '=', state.id)
    #         )
    #
    #     district = self.district_id
    #     if district:
    #         domain.append(
    #             ('district_id', '=', district.id)
    #         )
    #     ward = self.ward_id
    #     if ward:
    #         domain.append(
    #             ('ward_id', '=', ward.id)
    #         )
    #     management_unit = self.management_unit_id
    #     if management_unit:
    #         domain.append(
    #             ('partner_id', '=', management_unit.id)
    #         )
    #
    #     domain.append(
    #         ('from_date', '>=', from_date),
    #     )
    #     domain.append(
    #         ('from_date', '<=', to_date),
    #     )
    #
    #     if national == 'vietname'
    #         domain.append(
    #             ()
    #         )
    #
    #     return domain

    @api.multi
    def get_domain(self):
        from_date = datetime.datetime.strptime(self.from_date, '%Y-%m-%d')
        to_date = datetime.datetime.strptime(self.to_date, '%Y-%m-%d')
        if from_date > to_date:
            raise UserError(_(
                'Error: '
                'You can not export report with \
                 from date greater than to date'))
        domain = []
        state_id = self.state_id.id
        district_id = self.district_id.id
        ward_id = self.ward_id.id
        management_unit_id = self.management_unit_id.id
        from_date = self.from_date + ' 00:00:00'
        from_date = self.change_utc_to_local_datetime(from_date)
        to_date = self.to_date + ' 23:59:59'
        to_date = self.change_utc_to_local_datetime(to_date)
        national = self.national
        vn_country_id = self.env.ref('base.vn').id
        if state_id:
            domain.append(
                ('state_id', '=', state_id)
            )
        if district_id:
            domain.append(
                ('district_id', '=', district_id)
            )
        if ward_id:
            domain.append(
                ('ward_id', '=', ward_id)
            )
        if management_unit_id:
            domain.append(
                ('partner_id', '=', management_unit_id)
            )

        domain.append(
            ('from_date', '>=', from_date),
        )
        domain.append(
            ('from_date', '<=', to_date),
        )

        if national == 'vietnamese':
            domain.append(
                ('country_id', '=', vn_country_id)
            )

        if national == 'foreigner':
            domain.append(
                ('country_id', '!=', vn_country_id)
            )
        return domain

    @api.multi
    def view_report(self):
        action = self.env.ref(
            'bms_guest_stay_report.action_report_notebook_of_stay_tree')
        action_view = action.read()
        domain = self.get_domain()
        action_view[0]['domain'] = domain
        return action_view[0]
