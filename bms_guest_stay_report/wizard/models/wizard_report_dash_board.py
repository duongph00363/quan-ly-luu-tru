# -*- coding: utf-8 -*-
from odoo import fields, models, api


class WizardReportDashBoard(models.TransientModel):
    _name = 'wizard.report.dash.board'
    _description = 'Wizard report dashboard'

    def _state_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return self.env.user.state_id.id
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.state_id.id
        return False

    def _district_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.district_id.id
        return False

    def _ward_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.ward_id.id
        return False

    def _management_unit_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            management_unit_ids = self.env.user.management_unit_ids
            return management_unit_ids and management_unit_ids[0].id or False
        return False

    def _is_super_manager_default(self):
        res = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        return res

    management_unit_state_id = fields.Many2one('res.country.state',
                                               string='State',
                                               default=_state_id_default)
    management_unit_district_id = fields.Many2one('res.country.district',
                                                  string='District',
                                                  default=_district_id_default)
    management_unit_ward_id = fields.Many2one('res.country.ward',
                                              string='Ward',
                                              default=_ward_id_default)
    management_unit_id = fields.Many2one('bms.management.unit',
                                         'Management unit',
                                         default=_management_unit_id_default)
    is_super_manager = fields.Boolean(compute='_compute_is_super_manager',
                                      default=_is_super_manager_default)

    @api.model
    def _compute_is_super_manager(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        self.is_super_manager = is_super_manager

    @api.multi
    def management_unit_onchange(
            self, management_unit_state_id, management_unit_district_id,
            management_unit_ward_id, context={}):

        res = {
            'value': {}
        }
        if management_unit_state_id:
            res['domain'] = {
                'management_unit_id':
                    [('state_id', '=', management_unit_state_id)]
            }

        if management_unit_district_id:
            res['domain'] = {
                'management_unit_id':
                    [('district_id', '=', management_unit_district_id)]
            }

        if management_unit_ward_id:
            res['domain'] = {
                'management_unit_id':
                    [('ward_id', '=', management_unit_ward_id)]
            }

        if context.get('change_state', False):
            res['value']['management_unit_district_id'] = False
            res['value']['management_unit_ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_district', False):
            res['value']['management_unit_ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_ward', False):
            res['value']['management_unit_id'] = False

        return res

    @api.multi
    def get_domain(self):
        domain = []
        management_unit_state_id = self.management_unit_state_id.id
        management_unit_district_id = self.management_unit_district_id.id
        management_unit_ward_id = self.management_unit_ward_id.id
        management_unit_id = self.management_unit_id.id
        if management_unit_state_id:
            domain.append(
                ('management_unit_state_id', '=', management_unit_state_id)
            )
        if management_unit_district_id:
            domain.append(
                ('management_unit_district_id', '=',
                 management_unit_district_id)
            )
        if management_unit_ward_id:
            domain.append(
                ('management_unit_ward_id', '=', management_unit_ward_id)
            )
        if management_unit_id:
            domain.append(
                ('management_unit_id', '=', management_unit_id)
            )

        return domain

    @api.multi
    def view_report(self):
        action = self.env.ref(
            'bms_guest_stay_report.action_report_dash_board_kanban_view')
        action_view = action.read()
        domain = self.get_domain()
        action_view[0]['context'] = {'domain': domain}
        return action_view[0]
