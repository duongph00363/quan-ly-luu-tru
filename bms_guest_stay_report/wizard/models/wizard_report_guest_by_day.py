# -*- coding: utf-8 -*-
from odoo import fields, models, api, _, exceptions
import datetime
from dateutil.relativedelta import relativedelta
import pytz
from datetime import timedelta
from odoo.tools.misc import DEFAULT_SERVER_DATE_FORMAT


class WizardReportGuestByDay(models.TransientModel):
    _name = 'wizard.report.guest.by.day'
    _description = 'Wizard report guest by day'
    _rec_name = 'id'

    def default_from_date(self):
        date_now = datetime.datetime.now()
        date_now = date_now.strftime('%Y-%m') + '-01'
        return date_now

    def default_to_date(self):
        date_now = datetime.datetime.now() + relativedelta(months=1)
        date_now = date_now.strftime('%Y-%m') + '-01'
        date_now = datetime.datetime.strptime(date_now, '%Y-%m-%d')
        date_now = date_now + relativedelta(days=-1)
        date_now = date_now.strftime('%Y-%m-%d')
        return date_now

    def _state_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return self.env.user.state_id.id
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.state_id.id
        return False

    def _district_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.district_id.id
        return False

    def _ward_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.ward_id.id
        return False

    def _management_unit_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.partner_id.id
        return False

    def _is_super_manager_default(self):
        res = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        return res

    state_id = fields.Many2one('res.country.state', string='State',
                               default=_state_id_default)
    district_id = fields.Many2one('res.country.district', string='District',
                                  default=_district_id_default)
    ward_id = fields.Many2one('res.country.ward', string='Ward',
                              default=_ward_id_default)
    management_unit_id = fields.Many2one('res.partner',
                                         string='Management unit',
                                         default=_management_unit_id_default)

    from_date = fields.Date(required=True, default=default_from_date)
    to_date = fields.Date(required=True, default=default_to_date)
    state = fields.Selection([('all', 'All'),
                              ('have_guests', 'Have guests'),
                              ('no_guests', 'No guests')],
                             default='all', required=True,
                             string='Type')

    is_super_manager = fields.Boolean(compute='_compute_is_super_manager',
                                      default=_is_super_manager_default)

    @api.model
    def _compute_is_super_manager(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        self.is_super_manager = is_super_manager

    @api.multi
    def management_unit_onchange(
            self, state_id, district_id, ward_id, context={}):

        res = {
            'value': {}
        }
        if state_id:
            res['domain'] = {
                'management_unit_id': [('state_id', '=', state_id),
                                       ('is_management_unit', '=', True)]
            }

        if district_id:
            res['domain'] = {
                'management_unit_id':
                    [('district_id', '=', district_id),
                     ('is_management_unit', '=', True)]
            }

        if ward_id:
            res['domain'] = {
                'management_unit_id': [('ward_id', '=', ward_id),
                                       ('is_management_unit', '=', True)]
            }

        if context.get('change_state', False):
            res['value']['district_id'] = False
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_district', False):
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_ward', False):
            res['value']['management_unit_id'] = False

        return res

    @api.multi
    def change_utc_to_local_datetime(self, souce_date):
        user_tz = self.env.user.tz or str(pytz.utc)
        tz_now = datetime.datetime.now(pytz.timezone(user_tz))
        difference = tz_now.utcoffset().total_seconds() / 60 / 60
        difference = int(difference)
        utc_date = datetime.datetime.strptime(souce_date,
                                              '%Y-%m-%d %H:%M:%S')
        utc_date = utc_date + timedelta(hours=-difference)
        return utc_date.strftime('%Y-%m-%d %H:%M:%S')

    @api.multi
    def get_accommodation_querry(self):
        from_date = self.from_date + ' 00:00:00'
        utc_from_date = self.change_utc_to_local_datetime(from_date)
        to_date = self.to_date + ' 23:59:59'
        utc_to_date = self.change_utc_to_local_datetime(to_date)

        date_where_clause = """ g.from_date >= '{}'
                                and g.from_date <= '{}'
                            """.format(utc_from_date, utc_to_date)
        state_id_where_clause = ""
        if self.state_id:
            state_id_where_clause = """ and p.state_id = {}
            """.format(self.state_id.id)

        district_id_where_clause = ""
        if self.district_id:
            district_id_where_clause = """ and p.district_id = {}
            """.format(self.district_id.id)

        management_unit_id_where_clause = ""
        if self.management_unit_id:
            management_unit_id_where_clause = """ and p.id = {}
            """.format(self.management_unit_id.id)

        ward_id_where_clause = ""
        if self.ward_id:
            ward_id_where_clause = """and p.ward_id = {}
                    """.format(self.ward_id.id)

        state_where_clause = ""
        if self.state == 'no_guests':
            state_where_clause = """ and d.count_guest = 0"""
        elif self.state == 'have_guests':
            state_where_clause = """ and d.count_guest > 0"""

        querry = """
                    with d as (
                        select a.id as id,
                          a.name,
                          p.id as p_id,
                          a.phone,
                          a.street as accom_street,
                          rs.name as state_name,
                          rd.name as district_name,
                          rw.name as ward_name,
                          p.state_id as state_id,
                          p.district_id as district_id,
                          p.ward_id as ward_id,
                          count(g.id) as count_guest,
                          coalesce(a.street || ', ' , '') ||
                          coalesce(arw.name || ', ' , '') ||
                          coalesce(ard.name|| ', ', '')||
                          coalesce(ars.name, '') as accommodation_address
                        from bms_accommodation as a
                            left join bms_guest_stay as g
                            on g.accommodation_id = a.id
                            left join res_partner as p on p.id = a.partner_id
                            left join res_country_state
                            as rs on rs.id = p.state_id
                            left join res_country_district
                            as rd on rd.id = p.district_id
                            left join res_country_ward
                            as rw on rw.id = p.ward_id
                            left join res_country_state
                            as ars on ars.id = a.state_id
                            left join res_country_district
                            as ard on ard.id = a.district_id
                            left join res_country_ward
                            as arw on arw.id = a.ward_id
                        where {}
                        {} {} {} {}
                        group by a.id, a.name,
                        p.id, p.street, a.phone,
                        rs.name,
                        rd.name,
                        rw.name,
                        p.state_id,
                        p.district_id,
                        p.ward_id,
                        arw.name,
                        ard.name,
                        ars.name
                        )
                    select *
                    from d
                    where 1=1 {}
                """.format(date_where_clause, state_id_where_clause,
                           district_id_where_clause, ward_id_where_clause,
                           management_unit_id_where_clause, state_where_clause)

        return querry

    @api.multi
    def get_accommodation_dict(self):
        querry = self.get_accommodation_querry()
        self.env.cr.execute(querry)
        querry_result = self.env.cr.fetchall()
        accommodation_dict = {}
        state_dict = {}
        district_dict = {}
        ward_dict = {}
        for row in querry_result:
            accommodation_id = row[0]
            accommodation_name = row[1]
            management_unit_id = row[2]
            mobile = row[3]
            street = row[4]
            state_name = row[5] or _('Un define')
            district_name = row[6] or _('Un define')
            ward_name = row[7] or _('Un define')
            state_id = row[8]
            district_id = row[9]
            ward_id = row[10]
            count_guest = row[11]
            accommodation_address = row[12]

            if not accommodation_dict.get(state_id, False):
                accommodation_dict[state_id] = {}

            if not accommodation_dict[state_id].get(district_id, False):
                accommodation_dict[state_id][district_id] = {}

            if not accommodation_dict[state_id][district_id].get(ward_id,
                                                                 False):
                accommodation_dict[state_id][district_id][ward_id] = []

            if not state_dict.get(state_id, False):
                state_dict[state_id] = {
                    'count': 0,
                    'name': state_name
                }
            state_dict[state_id]['count'] += count_guest

            if not district_dict.get(district_id, False):
                district_dict[district_id] = {
                    'count': 0,
                    'name': district_name
                }
            district_dict[district_id]['count'] += count_guest

            if not ward_dict.get(ward_id, False):
                ward_dict[ward_id] = {
                    'count': 0,
                    'name': ward_name
                }
            ward_dict[ward_id]['count'] += count_guest

            accommodation_info = {
                'accommodation_id': accommodation_id,
                'accommodation_name': accommodation_name,
                'management_unit_id': management_unit_id,
                'mobile': mobile,
                'street': street,
                'state_name': state_name,
                'district_name': district_name,
                'ward_name': ward_name,
                'count_guest': count_guest,
                'accommodation_address': accommodation_address
            }
            accommodation_dict[state_id][district_id][ward_id].append(
                accommodation_info)

        return accommodation_dict, state_dict, district_dict, ward_dict

    @api.multi
    def export_report(self):
        datas = {'ids': self.ids}
        datas['model'] = 'wizard.report.guest.by.day'
        datas['form'] = self.read()[0]
        for field in datas['form'].keys():
            if isinstance(datas['form'][field], tuple):
                datas['form'][field] = datas['form'][field][0]

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'bms_guest_stay.guest_by_day_xls.xlsx',
            'datas': datas,
            'name': _('Guest by day')
        }

    @api.multi
    def get_domain(self):
        domain = []
        state_id = self.state_id.id
        district_id = self.district_id.id
        ward_id = self.ward_id.id
        management_unit_id = self.management_unit_id.id
        from_date = self.from_date + ' 00:00:00'
        from_date = self.change_utc_to_local_datetime(from_date)
        to_date = self.to_date + ' 23:59:59'
        to_date = self.change_utc_to_local_datetime(to_date)
        if state_id:
            domain.append(
                ('state_id', '=', state_id)
            )
        if district_id:
            domain.append(
                ('district_id', '=', district_id)
            )
        if ward_id:
            domain.append(
                ('ward_id', '=', ward_id)
            )
        if management_unit_id:
            domain.append(
                ('partner_id', '=', management_unit_id)
            )

        domain.append(
            ('from_date', '>=', from_date),
        )
        # domain.append('|')
        # domain.append('&')

        domain.append(
            ('from_date', '<=', to_date),
        )
        # domain.append(
        #     ('to_date', '>=', from_date),
        # )
        # domain.append(
        #     ('to_date', '<=', to_date),
        # )
        return domain

    @api.multi
    def view_report(self):
        action = self.env.ref(
            'bms_guest_stay_report.action_report_guest_by_day_pivot_view')
        action_view = action.read()
        domain = self.get_domain()
        action_view[0]['domain'] = domain
        return action_view[0]

    @api.multi
    @api.constrains('from_date', 'to_date')
    def _check_date(self):
        for wizard in self:
            from_date = wizard.from_date
            from_date = datetime.datetime.strptime(from_date,
                                                   DEFAULT_SERVER_DATE_FORMAT)
            to_date = wizard.to_date
            to_date = datetime.datetime.strptime(to_date,
                                                 DEFAULT_SERVER_DATE_FORMAT)

            if from_date > to_date:
                raise exceptions.ValidationError(_(
                    'From date must be less than or equal to date!'))
        return True
