# -*- coding: utf-8 -*-
{
    # 'name': 'bms_guest_stay',
    'name': 'BMS Guest Stay Management',
    'version': '10.0.1.0.0',
    'author': 'BMS Group Global',
    'license': 'AGPL-3',
    'summary': 'BMS guest stay',
    'sequence': 30,
    'category': 'BMS Module',
    'website': 'http://bmsgroupglobal.com/',
    'images': [],
    'depends': ['base',
                'auth_signup_verify_email'],
    'data': [
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
