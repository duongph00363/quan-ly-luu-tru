# -*- coding: utf-8 -*-
from odoo import fields, models


class BmsWantedInformation(models.Model):
    _name = 'bms.wanted.information'
    _description = 'Wanted Information'

    name = fields.Char(required=True)
    other_name = fields.Char()
    gender = fields.Selection([('male', 'Male'),
                               ('female', 'Female'),
                               ('undefine', 'Undefine')],
                              default='male')
    birth_day = fields.Date()
    identification_number = fields.Char(required=True)
    passport_number = fields.Char()
    father_name = fields.Char()
    mother_name = fields.Char()

    state_id_resident = fields.Many2one('res.country.state',
                                        string='State',
                                        domain=lambda self:
                                        [('country_id', '=',
                                          self.env.ref('base.vn').id)])
    district_id_resident = fields.Many2one('res.country.district',
                                           string='District',
                                           )

    state_id_escape = fields.Many2one('res.country.state',
                                      string='State',
                                      domain=lambda self:
                                      [('country_id', '=',
                                        self.env.ref('base.vn').id)])
    district_id_escape = fields.Many2one('res.country.district',
                                         string='District')
    time_escape = fields.Date()
    address_escape = fields.Char()
    native_country = fields.Char()
    address_resident = fields.Char()
    character_identity = fields.Char()
    charge = fields.Char()
    unit_wanted = fields.Char()
    warrant = fields.Char()
    # attachment_id = fields.Many2one('ir.attachment', string='Picture',
    #                                 required=True, ondelete='cascade')
    attachment_id = fields.Many2many(
        'ir.attachment', 'bms_wanted_information_attachment_rel',
        'bms_wanted_information_id', 'attachment_id', string='Picture')
    # attachment_id = fields.Binary("Photo", attachment=True)
    finger_prints_id = fields.Many2many(
        'ir.attachment', 'bms_wanted_information_finger_prints_rel',
        'bms_wanted_information_id', 'finger_prints_id',
        string='Finger Prints')
