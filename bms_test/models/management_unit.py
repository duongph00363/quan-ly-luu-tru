# -*- coding: utf-8 -*-
from odoo import models, fields, api


@api.model
def _lang_get(self):
    return self.env['res.lang'].get_installed()


class BmsManagementUnit(models.Model):
    _name = 'bms.management.unit'
    _description = 'Management Unit'

    name = fields.Char(required=True)
    # image = fields.Binary("Image", attachment=True,
    #                       help="This field holds the image
    # used as avatar for "
    #                            "this contact, limited to 1024x1024px", )
    # active = fields.Boolean(defaul=True)
    user_id = fields.Many2one('res.users', 'User Registry', readonly=True,
                              copy=False)
    street = fields.Char()
    country_id = fields.Many2one('res.country', string='Country',
                                 required=True,
                                 default=lambda self: self.env.ref('base.vn'))
    state_id = fields.Many2one('res.country.state',
                               string='State',
                               domain=lambda self:
                               [('country_id', '=',
                                 self.env.ref('base.vn').id)])
    district_id = fields.Many2one('res.country.district',
                                  string='District')
    ward_id = fields.Many2one('res.country.ward',
                              string='Ward')
    zip = fields.Char()
    website = fields.Char(help="Website of Management Unit")
    phone = fields.Char()
    mobile = fields.Char()
    email = fields.Char(required=True)
    fax = fields.Char()
    lang = fields.Selection(_lang_get, string='Language',
                            default=lambda self: self.env.lang,
                            help="If the selected language is loaded in "
                                 "the system, all documents related to "
                                 "this contact will be printed in "
                                 "this language. If not, it will be English.")
    note = fields.Text()
    active = fields.Boolean(default=True)

    @api.multi
    def create_user(self):
        user_value = {
            'name': self.name,
            'login': self.email,
            'password': '123456',
            'active': True,
            'is_company': True,
            'groups_id': [(6, False, [self.env.ref(
                'bms_guest_stay.group_e_guest_principal_official').id])],
            'street': self.street,
            'country_id': self.country_id.id,
            'state_id': self.state_id.id,
            'district_id': self.district_id.id,
            'ward_id': self.ward_id.id,
            'website': self.website,
            'phone': self.phone,
            'mobile': self.mobile,
            'fax': self.fax,
            'email': self.email,
            'lang': self.lang,
            'is_management_unit': True,
            'category_id': [(6, False, [self.env.ref(
                'bms_guest_stay.res_partner_category_management_unit').id])],
        }
        user_id = self.env['res.users'].create(user_value)
        self.write({'user_id': user_id.id})
        return user_id

    @api.multi
    def write(self, vals):
        fields_list = ['name', 'state_id', 'district_id', 'ward_id',
                       'website', 'phone', 'mobile', 'fax', 'lang']
        fields_list_change = []
        for field in vals.keys():
            if field in fields_list:
                fields_list_change.append(field)

        if fields_list_change:
            partner_vals = dict((f, vals[f]) for f in fields_list_change)
            self[0].user_id.partner_id.write(partner_vals)
        return super(BmsManagementUnit, self).write(vals)
