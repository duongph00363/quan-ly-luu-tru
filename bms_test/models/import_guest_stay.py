# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
import datetime
from odoo.exceptions import ValidationError
import ast
import copy


class ImportGuestStayLine(models.Model):
    _name = 'import.guest.stay.line'
    _description = 'Import Guest stay line'

    row_number = fields.Integer()
    import_id = fields.Char(string='Import')
    name = fields.Char()
    receiver = fields.Char()
    reason = fields.Char()
    accommodation_id = fields.Char(string='Accommodation')
    type = fields.Char()
    room = fields.Char()
    from_date = fields.Char()
    to_date = fields.Char()
    user_id = fields.Char(string='User enter')
    overseas_vietnamese = fields.Char()
    identification_type = fields.Char()
    identification_number = fields.Char()
    passport_type = fields.Char()
    visa = fields.Char()
    sign = fields.Char()
    duration = fields.Char()
    date_of_issue = fields.Char()
    place_of_issue = fields.Char()
    partner_id = fields.Char(string='Customer')
    create_partner = fields.Char()
    street = fields.Char()
    country_id = fields.Char(string='Country')
    state_id = fields.Char(string='State')
    district_id = fields.Char(string='District')
    ward_id = fields.Char(string='Ward')
    zip = fields.Char()
    entry_date = fields.Date()
    border_gate = fields.Char()
    religion = fields.Char()
    gender = fields.Char()
    birth_day = fields.Date()
    nation = fields.Char()
    receiver = fields.Char()
    reason = fields.Char()
    note = fields.Char()
    error = fields.Char()


column_dict = {
    'name': [0,  _('Name'),  'char',  '',  '',  False, True],
    'receiver': [1,  _('Receiver'),  'char',  '',  '',  False, False],
    'reason': [2,  _('Reason'),  'm2o',
               'bms.guest.reason', 'name', False, False],
    'type': [3,  _('Type'),  'selection',  '',  '',  False, True],
    'room': [4,  _('Room'),  'char',  '',  '',  False, True],
    'from_date': [5,  _('From date'),  'date',  '',  '',  False, True],
    'to_date': [6,  _('To date'),  'date',  '',  '',  False, False],
    'user_id': [7,  _('User enter'),  'm2o',
                'res.users',  'name',  False, True],
    'overseas_vietnamese': [8,  _('Overseas vietnamese'),
                            'boolean',  '',  '',  False, False],
    'identification_type': [9,  _('Identification type'),
                            'selection',  '',  '',  False, True],
    'identification_number': [10,  _('Identification number'),
                              'char', '', '', False, True],
    'passport_type': [11,  _('Passport type'),
                      'selection',  '',  '',  False, False],
    'visa': [12,  _('Visa'),  'char',  '',  '',  False, False],
    'sign': [13,  _('Sign'),  'char',  '',  '',  False, False],
    'duration': [14,  _('Duration'),  'date',  '',  '',  False, False],
    'date_of_issue': [15,  _('Date of issue'),
                      'char',  '',  '',  False, False],
    'place_of_issue': [16,  _('Place of issue'),
                       'char',  '',  '',  False, False],
    'partner_id': [17,  _('Customer'),  'm2o',
                   'res.partner',  'name',  False, False],
    'create_partner': [18,  _('Create partner'),
                       'boolean',  '',  '',  False, False],
    'street': [19,  _('Street'),  'char',  '',  '',  False, False],
    'country_id': [20,  _('Country'),  'm2o',
                   'res.country',  'name',  False, True],
    'state_id': [21,  _('State'),  'm2o',
                 'res.country.state',  'name',  False, False],
    'district_id': [22,  _('District'),  'm2o',
                    'res.country.district',  'name',  False, False],
    'ward_id': [23,  _('Ward'),  'm2o',
                'res.country.ward',  'name',  False, False],
    'zip': [24,  _('Zip'),  'char',  '',  '',  False, False],
    'entry_date': [25,  _('Entry date'),  'date',  '',  '',  False, False],
    'border_gate': [26,  _('Border gate'),  'm2o',
                    'bms.guest.border.gate',  'name',  False, False],
    'religion': [27,  _('Religion'),  'char',  '',  '',  False, False],
    'gender': [28,  _('Gender'),  'selection',  '',  '',  False, False],
    'birth_day': [29,  _('Birth day'),  'date',  '',  '',  False, False],
    'nation': [30,  _('Nation'),  'char',  '',  '',  False, False],
    'note': [31,  _('Note'),  'char',  '',  '',  False, False],
}
MIN_COL_NUMBER = 32


class ImportGuestStay(models.Model):
    _name = 'import.guest.stay'
    _description = 'Import Guest stay'
    _inherits = {'ir.attachment': 'attachment_id'}
    _import_model_name = 'bms.guest.stay'
    _import_date_format = '%d-%m-%Y'

    attachment_id = fields.Many2one('ir.attachment', string='Attachment',
                                    required=True, ondelete='cascade')
    accommodation_id = fields.Many2one('bms.accommodation',
                                       string='Accommodation',
                                       required=True)
    user_id = fields.Many2one('res.users', string='Executor')
    template_file_url = fields.Char(compute='_compute_template_file_url',
                                    default=lambda self:
                                    self.env['ir.config_parameter'].
                                    get_param('web.base.url') +
                                    '/bms_guest_stay/static/import_template/'
                                    'import_guest_stay_template.xls')
    state = fields.Selection([('draft', 'Draft'),
                              ('validated', 'Validated'),
                              ('done', 'Done'),
                              ('cancel', 'Cancel')],
                             default='draft')
    line_ids = fields.One2many('import.guest.stay.line',
                               'import_id', readonly=True,
                               string='Lines')
    values_list = fields.Char(readonly=True)

    @api.multi
    def _compute_template_file_url(self):
        Parameters = self.env['ir.config_parameter']
        base_url = Parameters.get_param('web.base.url')
        url = base_url\
            + '/bms_guest_stay/static/import_template' \
            '/import_guest_stay_template.xls'
        for import_guest in self:
            import_guest.template_file_url = url

    @api.multi
    def get_default_value(self):
        return {
            'accommodation_id': self[0].accommodation_id.id,
        }

    def tr(self, source, field):
        uid = self.env.uid
        user = self.env['res.users'].browse(uid)
        lang = user.partner_id.lang

        self.env.cr.execute(
            '''select value
                from ir_translation
                where lang=%s and type='selection'
                and src=%s and name=%s''',
            (lang, source, self._import_model_name + ',' + field))
        res_trans = self.env.cr.fetchone()
        res = res_trans and res_trans[0] or False
        return res

    @api.multi
    def clear_header_footer(self, excell_data):
        del excell_data[0]
        return excell_data

    @api.model
    def check_selection_value(self, field_name, value):
        uid = self.env.uid
        user = self.env['res.users'].browse(uid)
        import_object = self.env[self._import_model_name].\
            with_context(lang=user.partner_id.lang)
        import_field = import_object._fields[field_name]
        selection = import_field.selection

        selection_label_dict = dict((self.tr(s[1], field_name),
                                     s[0]) for s in selection)

        # if field_name == 'identification_type':
        #     print 'identification_type'
        #     print value
        #     print selection_label_dict

        if value not in selection_label_dict.keys():
            return False
        return selection_label_dict[value]

    # Import button
    @api.multi
    def validate(self):
        if self.state != 'draft':
            return True

        # read excell file
        data = self.datas
        sheet = False

        user_id = self.env.uid
        user = self.env['res.users'].browse(user_id)
        path = '/import_employee_' + user.login + '_' + str(
            self[0].id) + '_' + str(datetime.datetime.now()) + '.xls'
        path = path.replace(' ', '_')

        read_excel_obj = self.env['read.excel']
        excel_data = read_excel_obj.read_file(data, sheet, path)

        #         check cotent excell data
        if len(excel_data) < 2:
            raise ValidationError(_('Cannot import empty file !'))

        # clear header footer
        self.clear_header_footer(excel_data)

        #         check number column excell file
        if len(excel_data[0]) < MIN_COL_NUMBER:
            raise ValidationError(_(
                'Format file incorrect, you must import file have at least {} '
                'column !'.format(MIN_COL_NUMBER)))

        excel_data2 = copy.deepcopy(excel_data)

        # check format data in excell file(unicode, str, ...)
        error_dict = self.verify_excel_data(excel_data)

        # Get db data dict for many2one, many2many field: dict with key = name,
        # value = ID db
        db_data = self.get_db_data(excel_data)

        # Get values list to create from excel data map with db data
        values_list = []

        row_num = 1
        for row in excel_data:
            row_num += 1
            values = self.get_value_from_excel_row(row, row_num,
                                                   db_data, error_dict)
            values_list.append(values)

        self.create_temp_import_record(excel_data2, error_dict)

        return self.write({'state': 'validated',
                           'values_list': unicode(values_list)})

    @api.multi
    def create_temp_import_record(self, excel_data, error_dict):
        lines_values = []
        row_num = 1
        for row in excel_data:
            row_num += 1
            line_values = {
                'row_number': row_num
            }
            error = u''

            for col_name in column_dict.keys():
                col_index = column_dict[col_name][0]
                line_values[col_name] = row[col_index]
                col_string = column_dict[col_name][1]
                if error_dict.get(row_num, {}).get(col_name, False):
                    error = error + '\n' + _(col_string) + ': ' + \
                        error_dict.get(row_num, {}).get(col_name, '')

            if error:
                line_values['error'] = error

            lines_values.append(
                (0, 0, line_values)
            )
        self.write(
            {'line_ids': lines_values}
        )

        return True

    @api.multi
    def verify_excel_data(self, excel_data):
        error_dict = {}
        row_count = 1

        for row in excel_data:
            row_count += 1

            # verify for each cell
            for column_name in column_dict.keys():
                col_index = column_dict[column_name][0]
                col_type = column_dict[column_name][2]
                required = column_dict[column_name][6]

                if not row[col_index] and required:
                    error_dict[row_count][column_name] = \
                        _('value blank, column is required')
                    continue

                try:
                    if col_type in ('m2o', 'm2m'):
                        row[col_index] = unicode(row[col_index]).strip()
                    else:
                        row[col_index] = unicode(row[col_index]).strip()
                except Exception:
                    if not error_dict.get(row_count, False):
                        error_dict[row_count] = {}
                    error_dict[row_count][
                        column_name] = \
                        _('value invalid or contains special characters!')
                    continue

                try:
                    if (col_type == 'date') and (row[col_index] != ""):
                        row[col_index] = datetime.datetime.strptime(
                            row[col_index], self._import_date_format
                        ).strftime('%Y-%m-%d')
                except Exception:
                    if not error_dict.get(row_count, False):
                        error_dict[row_count] = {}
                    error_dict[row_count][
                        column_name] = \
                        _('date type column must be has'
                          ' "day-month-year" format!')
                    continue

                if col_type == 'boolean' \
                        and row[col_index].strip() in ('1', '0'):
                    if not error_dict.get(row_count, False):
                        error_dict[row_count] = {}
                    error_dict[row_count][
                        column_name] = \
                        _('value invalid, value must be "1" or "0"!')
                    continue

                if col_type == 'selection' and row[col_index]:
                    check = self.check_selection_value(column_name,
                                                       row[col_index])
                    if not error_dict.get(row_count, False):
                        error_dict[row_count] = {}
                    if check:
                        row[col_index] = check
                    else:
                        error_dict[row_count][
                            column_name] = \
                            _('value invalid, value must be a'
                              ' value of selection in entry form!')
                        continue

        return error_dict

    @api.multi
    def get_db_data(self, excel_data):
        res = {}
        for col in column_dict.keys():
            col_index = column_dict[col][0]
            # col_name = column_dict[col][1]
            col_type = column_dict[col][2]
            col_model = column_dict[col][3]
            col_field_name = column_dict[col][4]
            flag_load_all = column_dict[col][5]
            fieldname = col.lower().replace('index_', '')

            if col_type not in ('m2o', 'm2m'):
                continue

            if flag_load_all:
                db_data_s = self.env[col_model].search_read([],
                                                            [col_field_name])
                res[fieldname] = dict(
                    (row[col_field_name], row['id']) for row in db_data_s)

                continue

            name_list = [row[col_index] for row in excel_data]

            # table = self.env[col_model]._table

            db_data_s = self.env[col_model].search_read([
                ('{}'.format(col_field_name), 'in', name_list)
            ], [col_field_name])

            res[fieldname] = dict(
                (row[col_field_name], row['id']) for row in db_data_s)

        return res

    def create_partner(self, guest_stay):
        partner_value = {
            'name': guest_stay.name,
            'identification_number': guest_stay.identification_number,
            'country_id': self.env.ref('base.vn').id,
            'state_id': guest_stay.state_id.id,
            'district_id': guest_stay.district_id.id,
            'ward_id': guest_stay.ward_id.id,
            'zip': guest_stay.zip
        }
        partner = self.env['res.partner'].create(partner_value)
        guest_stay.write({'partner_id': partner.id})
        return True

    @api.multi
    def get_value_from_excel_row(self, row, row_num, db_data, error_dict):
        values = {}

        for col in column_dict.keys():
            field_name = col.replace('INDEX_', '').lower()
            col_index = column_dict[col][0]
            col_name = column_dict[col][1]
            col_type = column_dict[col][2]

            if not row[col_index]:
                values[field_name] = False
                continue

            if col_type not in ('m2o', 'm2m'):
                values[field_name] = row[col_index]
                continue

            if col_type == 'm2o':
                excel_value = row[col_index].strip()
                db_value = db_data[field_name].get(excel_value, False)

                if not db_value:
                    # Hard code Tạo khách hàng tự động khi không tìm thấy
                    create_partner_index = column_dict['create_partner'][0]
                    create_partner = row[create_partner_index]
                    if field_name == 'partner_id' and create_partner:
                        continue

                    old_error = error_dict.get(row_num, {}).get(field_name, '')
                    error_dict[row_num][field_name] = \
                        old_error + \
                        _(u''', Not found "{}" in row {}!
                        ''').format(excel_value, row_num)

                values[field_name] = db_value
                continue

            if col_type == 'm2m':
                excel_value = row[col_index]
                excel_value_list = excel_value.split(';')

                values[field_name] = [(6, False, [])]

                for sub_excel_value in excel_value_list:
                    sub_excel_value = sub_excel_value.strip()

                    sub_db_value = db_data[field_name].get(sub_excel_value,
                                                           False)

                    if not sub_db_value:
                        old_error = error_dict.get(row_num, {}).get(field_name,
                                                                    '')
                        error_dict[row_num][field_name] = \
                            old_error + \
                            _(u''', Not found {} = "{}" in row {} !
                            ''').format(col_name, excel_value, row_num)
                        # raise ValidationError(
                        #     _('Not found {} = "{}" in row {} '
                        #       '!'.format(col_name, sub_excel_value,
                        #                  row_num)))

                    values[field_name][0][2].append(sub_db_value)

        return values

    # Create record from values list
    @api.multi
    def create_record(self):
        if self.state != 'validated':
            return True
        error_line = self.env['import.guest.stay.line'].search(
            [('error', '!=', False), ('import_id', '=', self[0].id)])
        if error_line:
            raise ValidationError(
                _('Cannot create record when has error line!'))

        #         remove '.0'
        def remove_decimal_part(value):
            if not value:
                return value

            temp_list = unicode(value).split('.')
            if len(temp_list):
                value = temp_list[0]
            return value

        default_value = self.get_default_value() or {}
        model_name_obj = self.env[self._import_model_name]
        create_record_ids = []
        values_list = ast.literal_eval(self.values_list)
        for values in values_list:
            default_value.update(values)
            guest_stay = model_name_obj.create(default_value)
            if not default_value.get('partner_id', False) \
                    and default_value.get('create_partner', False):
                self.create_partner(guest_stay)

            create_record_ids.append(guest_stay.id)

        self.write({'state': 'done'})
        return True

    @api.multi
    def cancel(self):
        self.write({'state': 'cancel'})
