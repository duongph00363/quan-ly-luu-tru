# -*- coding: utf-8 -*-
from odoo import fields, models


class ResPartner(models.Model):
    _inherit = 'res.users'

    management_unit_ids = fields.One2many('bms.management.unit',
                                          'user_id',
                                          readonly=True)
