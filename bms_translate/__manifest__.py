# -*- coding: utf-8 -*-
{
    'name': 'bms_translate',
    'version': '10.0.1.0.0',
    'author': 'BMS Group Global',
    'license': 'AGPL-3',
    'summary': 'translate module base',
    'sequence': 30,
    'category': 'BMS Module',
    'website': 'http://bmsgroupglobal.com/',
    'images': [],
    'depends': ['base',
                'web'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
