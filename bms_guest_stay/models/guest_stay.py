# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import _, api, fields, models
from odoo.exceptions import UserError


class GuestStay(models.Model):
    _name = 'bms.guest.stay'
    _description = 'Guest stay'

    def _is_super_manager_default(self):
        res = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        return res

    type = fields.Selection([('odd', 'Odd'),
                             ('group', 'Group')],
                            default=lambda self: self.type_default())
    flag_readonly = fields.Selection([('o', 'O'),
                                      ('g', 'G')], default=lambda self: self.type_readonly())
    name = fields.Char(required=True)
    group_id = fields.Many2one('res.partner', 'Group')
    overseas_vietnamese = fields.Boolean(default=False)
    receiver = fields.Char()
    reason = fields.Many2one('bms.guest.reason')
    street = fields.Char()
    zip = fields.Char()
    country_id = fields.Many2one('res.country', string='Country',
                                 required=True,
                                 default=lambda self: self.env.ref('base.vn'))
    state_id = fields.Many2one('res.country.state',
                               string='State',
                               domain=lambda self:
                               [('country_id', '=',
                                 self.env.ref('base.vn').id)])
    district_id = fields.Many2one('res.country.district',
                                  string='District')
    ward_id = fields.Many2one('res.country.ward',
                              string='Ward')
    is_entry = fields.Boolean(compute='_compute_entry', store=True,
                              defaul=False)
    entry_date = fields.Date()
    border_gate = fields.Many2one('bms.guest.border.gate')

    religion = fields.Char()
    gender = fields.Selection([('male', 'Male'),
                               ('female', 'Female'),
                               ('undefine', 'Un define')],
                              default='male')
    birth_day = fields.Date()
    nation = fields.Char()
    identification_type = fields.Selection([('identity_card',
                                             _('Identity card')),
                                            ('passport', _('Passport')),
                                            ('driving_license',
                                             _('Driving license'))],
                                           required=True,
                                           default='identity_card')
    identification_number = fields.Char(required=True)
    passport_type = fields.Selection(
        [('ordinary_passport', 'OR: Ordinary Passport'),
         ('official_passport', 'OF: Official Passport'),
         ('diplomatic_passport', 'DP: Diplomatic Passport')],
        default='ordinary_passport')
    visa = fields.Char()
    sign = fields.Char()
    duration = fields.Date(default=datetime.utcnow())
    date_of_issue = fields.Date()
    place_of_issue = fields.Many2one('res.partner',
                                     domain=[('is_company', '=', True)])
    partner_id = fields.Many2one('res.partner', 'Customer')
    create_partner = fields.Boolean(help='Create customer')

    accommodation_id = fields.Many2one('bms.accommodation',
                                       string='Accommodation',
                                       default=lambda self: self.default_accommodation_id())
    room = fields.Char(required=True)
    from_date = fields.Datetime(required=True,
                                default=datetime
                                .now().strftime('%Y-%m-%d %H:%M:%S'))
    to_date = fields.Datetime()

    date_checkout = fields.Datetime(readonly=True)
    from_date_gt = fields.Datetime(compute='_compute_from_date_gt')
    to_date_gt = fields.Datetime(compute='_compute_to_date_gt')
    date_checkout_gt = fields.Datetime(compute='_compute_date_checkout_gt')
    user_id = fields.Many2one('res.users', required=True,
                              string='User enter',
                              default=lambda self: self.env.user.id)
    note = fields.Text()
    # 0: state new, -1: check out, >0: stayed day number
    day_number = fields.Integer(default=0, readonly=True)
    # selection: "new", "checkout", "stayed DAY NUMBER"
    day_state = fields.Char(compute='_compute_state', store=True,
                            default='new', readonly=True)
    # date_now = fields.Datetime(compute='_compute_date_now')
    # state = fields.Char(compute='_compute_state')
    # day_state = fields.Char(readonly=True)

    # xu li de lay tinh huyen phuong xa cua
    # don vi chu quan dung cho view dashboard
    management_unit_state_id = \
        fields.Many2one('res.country.state',
                        compute='_compute_management_unit_state_id',
                        store=True)
    management_unit_district_id = \
        fields.Many2one('res.country.district',
                        compute='_compute_management_unit_district_id',
                        store=True)
    management_unit_ward_id = \
        fields.Many2one('res.country.ward',
                        compute='_compute_management_unit_ward_id',
                        store=True)
    management_unit_id = \
        fields.Many2one('res.partner',
                        compute='_compute_management_unit_id',
                        store=True)

    # xử lý để readonly field người nhập trên màn hình tạo khách lưu trú
    is_super_manager = fields.Boolean(compute='_compute_is_super_manager',
                                      default=_is_super_manager_default)
    check_wanted_obj = fields.Boolean(string="check wanted object",
                                      default=False)

    @api.model
    def _compute_is_super_manager(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        self.is_super_manager = is_super_manager

    @api.multi
    @api.depends('country_id')
    def _compute_entry(self):
        country_vn_id = self.env.ref('base.vn').id
        for guest in self:
            if guest.country_id.id == country_vn_id:
                guest.is_entry = False
            else:
                guest.is_entry = True

    @api.multi
    @api.depends('day_number')
    def _compute_state(self):
        for guest in self:
            if guest.day_number == 0:
                guest.day_state = _('new')
            elif guest.day_number == -1:
                guest.day_state = _('checkout')
            else:
                guest.day_state = \
                    _('stayed ') + str(guest.day_number) + (' day')

    @api.multi
    def checkout(self):
        self.write({'day_number': -1})
        self.date_checkout = datetime.now()

    @api.model
    def type_default(self):
        if self.env.user.accommodation_ids.accommodation_type == 'nha_dan':
            return 'odd'

    @api.model
    def type_readonly(self):
        if self.env.user.accommodation_ids.accommodation_type == 'nha_dan':
            return 'o'

    @api.model
    def default_accommodation_id(self):
        return self.env.user.accommodation_ids

    @api.model
    def recalculate_day_number(self):
        querry = u"""
            update bms_guest_stay
            set day_number = (now()::date - from_date::date + 1),
              day_state = %s || (now()::date-from_date::date + 1)::text || %s
            where day_number != -1
        """

        self.env.cr.execute(querry, [_(u'stayed '), _(u' day')])
        return True

    @api.onchange('identification_number')
    def identification_number_change(self):
        if self.identification_number:
            partner_s = self.env['res.partner']. \
                search([('identity_card', '=', self.identification_number)])
            self.partner_id = partner_s and partner_s[0] or False
            wanted_obj = self.env['bms.wanted.information']. \
                search([('identification_number', '=',
                         self.identification_number)])
            if len(wanted_obj) > 0:
                self.check_wanted_obj = True
            else:
                self.check_wanted_obj = False

    @api.onchange('partner_id')
    def partner_id_change(self):
        if self.partner_id:
            for field in ['state_id', 'district_id',
                          'ward_id', 'zip', 'street']:
                att_value = getattr(self.partner_id, field)
                if att_value:
                    setattr(self, field, att_value)

    @api.onchange('create_partner')
    def create_partner_change(self):
        if not self.partner_id and self.create_partner and self.name:
            self.create_partner = False
            single_customer_tag_id = self.env.ref(
                'bms_guest_stay.res_partner_category_single_customer').id
            partner_value = {
                'name': self.name,
                'identity_card': self.identification_number,
                'country_id': self.env.ref('base.vn').id,
                'state_id': self.state_id.id,
                'district_id': self.district_id.id,
                'ward_id': self.ward_id.id,
                'zip': self.zip,
                'is_single_customer': True,
                'category_id': [(6, False, [single_customer_tag_id])]
            }
            if self.type == 'group' and self.group_id:
                partner_value['parent_id'] = self.group_id.id
            partner_id = self.env['res.partner'].create(partner_value)
            self.partner_id = partner_id

    @api.onchange('country_id')
    def country_id_change(self):
        country_vn_id = self.env.ref('base.vn').id
        if self.country_id and self.country_id.id == country_vn_id:
            self.is_entry = False
        else:
            self.is_entry = True

    # define to fix CI: Duplicate xml field "from_date" - search datetime.
    @api.multi
    def _compute_from_date_gt(self):
        return 0

    @api.multi
    def _compute_to_date_gt(self):
        return 0

    @api.multi
    def _compute_date_checkout_gt(self):
        return 0

    # example change state.
    # @api.depends('name')
    # def _compute_date_now(self):
    #     for guest in self:
    #         guest.date_now = datetime.now()
    #
    # @api.depends('date_now', 'from_date', 'day_number')
    # def _compute_state(self):
    #     for guest in self:
    #         date_now = guest.date_now and datetime.strptime(
    #             guest.date_now,
    #           DEFAULT_SERVER_DATETIME_FORMAT).date() or datetime.now().date()
    #         from_date = datetime.strptime(
    #             guest.from_date or guest.create_date,
    #             DEFAULT_SERVER_DATETIME_FORMAT).date()
    #         if guest.day_number < 0:
    #             guest.state = _('Check Out')
    #         elif date_now == from_date:
    #             guest.state = _('new')
    #         else:
    #             delta = date_now - from_date
    #             day_delta = delta.days + 1
    #             guest.state = _('stayed ') + str(day_delta) + _(' day')
    #     self.env['bms.guest.stay'].call_func_update_state()
    #
    # @api.model
    # def call_func_update_state(self):
    #     stay_ids = self.env['bms.guest.stay'].search(
    #         [('state', '!=', _('Check Out'))])
    #     for s in stay_ids:
    #         s.write({'day_state': s.state})

    def unlink(self):
        raise UserError(_('Error: ''You can not delete record!'))
        return super(GuestStay, self).unlink()

    @api.onchange('identification_number')
    def _check_guest(self):
        if self.identification_number:
            domain = [('identification_number', '=',
                       self.identification_number),
                      ('day_state', 'not in', (_('Check Out'), _('new')))]
            count = self.search_count(domain)
            if count:
                return {
                    _('warning'): {
                        'title': _("Guest is staying"),
                        'message': _("The guest is staying in \
                        your accommodation or other accommodation. \
                        Please check your system again or call the manager! \
                        CONTINUE if the guest want to book room \
                        for the next time.")
                    },
                }

    def get_query(self, type):
        if type == 1:
            query = """
                select g.id,
                    m.state_id
                from bms_guest_stay as g
                    left join bms_accommodation as a
                    on a.id = g.accommodation_id
                    left join res_partner as p
                    on p.id = a.partner_id
                    left join res_users as u
                    on u.partner_id = p.id
                    left join bms_management_unit as m
                    on m.user_id = u.id
                where g.id in {}
            """.format(str(tuple(self.ids)).
                       replace('(,', '(').replace(',)', ')'))
        if type == 2:
            query = """
                        select g.id,
                            m.district_id
                        from bms_guest_stay as g
                            left join bms_accommodation as a
                            on a.id = g.accommodation_id
                            left join res_partner as p
                            on p.id = a.partner_id
                            left join res_users as u
                            on u.partner_id = p.id
                            left join bms_management_unit as m
                            on m.user_id = u.id
                        where g.id in {}
                    """.format(str(tuple(self.ids)).
                               replace('(,', '(').replace(',)', ')'))

        if type == 3:
            query = """
                        select g.id,
                            m.ward_id
                        from bms_guest_stay as g
                            left join bms_accommodation as a
                            on a.id = g.accommodation_id
                            left join res_partner as p
                            on p.id = a.partner_id
                            left join res_users as u
                            on u.partner_id = p.id
                            left join bms_management_unit as m
                            on m.user_id = u.id
                        where g.id in {}

                    """.format(
                str(tuple(self.ids)).replace('(,', '(').replace(',)', ')'))

        if type == 4:
            query = """
                        select g.id,
                            m.id
                        from bms_guest_stay as g
                            left join bms_accommodation as a
                            on a.id = g.accommodation_id
                            left join res_partner as p
                            on p.id = a.partner_id
                            left join res_users as u
                            on u.partner_id = p.id
                            left join bms_management_unit as m
                            on m.user_id = u.id
                        where g.id in {}

                    """.format(
                str(tuple(self.ids)).replace('(,', '(').replace(',)', ')'))

        return query

    @api.multi
    @api.depends('accommodation_id.partner_id.user_ids.management_unit_ids')
    def _compute_management_unit_state_id(self):
        query = self.get_query(1)
        self.env.cr.execute(query)
        query_result = self.env.cr.fetchall()
        query_result_dict = dict((row[0], row[1]) for row in query_result)
        for guest in self:
            guest.management_unit_state_id = \
                query_result_dict.get(guest.id, False)

    @api.multi
    @api.depends('accommodation_id.partner_id.user_ids.management_unit_ids')
    def _compute_management_unit_district_id(self):
        query = self.get_query(2)
        self.env.cr.execute(query)
        query_result = self.env.cr.fetchall()
        query_result_dict = dict((row[0], row[1]) for row in query_result)
        for guest in self:
            guest.management_unit_district_id = \
                query_result_dict.get(guest.id, False)

    @api.multi
    @api.depends('accommodation_id.partner_id.user_ids.management_unit_ids')
    def _compute_management_unit_ward_id(self):
        query = self.get_query(3)
        self.env.cr.execute(query)
        query_result = self.env.cr.fetchall()
        query_result_dict = dict((row[0], row[1]) for row in query_result)
        for guest in self:
            guest.management_unit_ward_id = \
                query_result_dict.get(guest.id, False)

    @api.multi
    @api.depends('accommodation_id.partner_id.user_ids.management_unit_ids')
    def _compute_management_unit_id(self):
        query = self.get_query(4)
        self.env.cr.execute(query)
        query_result = self.env.cr.fetchall()
        query_result_dict = dict((row[0], row[1]) for row in query_result)
        for guest in self:
            guest.management_unit_id = query_result_dict.get(guest.id, False)
