# -*- coding: utf-8 -*-
from odoo import fields, models, api


class BmsAccommodation(models.Model):
    _name = 'bms.accommodation'
    _description = 'Accommodation'

    def _partner_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.partner_id.id
        return False

    def _is_super_manager_default(self):
        res = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        return res

    name = fields.Char(required=True)
    active = fields.Boolean(default=True)
    partner_id = fields.Many2one('res.partner',
                                 'Management unit', required=True,
                                 default=_partner_id_default)
    contact_id = fields.Many2one('res.partner', 'Contact')
    user_id = fields.Many2one('res.users', 'User Registry', readonly=True,
                              copy=False)
    create_user = fields.Boolean(help='Create An User')
    tin = fields.Char('Tax identifier number')
    establishment_date = fields.Date()
    scale = fields.Selection([('guest_houses', 'Guest houses'),
                              ('hotel', 'Hotel'),
                              ('resort', 'Resort')])
    transaction_name = fields.Char()
    identity_card = fields.Char()
    business_license = fields.Char()
    phone = fields.Char()
    mobile = fields.Char()
    email = fields.Char(required=True)
    street = fields.Char()
    zip = fields.Char()
    country_id = fields.Many2one('res.country', string='Country',
                                 required=True,
                                 default=lambda self: self.env.ref('base.vn'))
    state_id = fields.Many2one('res.country.state', required=True,
                               string='State',
                               domain=lambda self:
                               [('country_id', '=',
                                 self.env.ref('base.vn').id)])
    district_id = fields.Many2one('res.country.district', required=True,
                                  string='District')
    ward_id = fields.Many2one('res.country.ward', required=True,
                              string='Ward')
    note = fields.Text()
    issued_date = fields.Date()
    issued_by = fields.Char()
    onebed_no = fields.Integer(string='Room with 1 bed')
    twobed_no = fields.Integer(string='Room with 2 bed')
    threebed_no = fields.Integer(string='Room with 3 bed')
    fourbed_no = fields.Integer(string='Room with 4 bed')
    totalbed_no = fields.Integer(compute='_compute_totalbed_no',
                                 string='Total room')

    # xử lý để readonly đơn vị chủ quản trên màn hình tạo cơ sở lưu trú
    is_super_manager = fields.Boolean(compute='_compute_is_super_manager',
                                      default=_is_super_manager_default)
    accommodation_type = fields.Selection(
        selection=[
            ('nha_dan', 'Nhà dân'),
            ('nha_nghi', 'Nhà nghỉ/khách sạn/nhà trọ')
        ],
        string='Kiểu lưu trú'
    )

    @api.model
    def _compute_is_super_manager(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        self.is_super_manager = is_super_manager

    @api.onchange('name')
    def _on_change_name(self):
        self.transaction_name = self.name

    @api.onchange('contact_id')
    def _on_change_contact_id(self):
        self.identity_card = self.contact_id.identity_card

    @api.depends('onebed_no', 'twobed_no', 'threebed_no', 'fourbed_no')
    @api.multi
    def _compute_totalbed_no(self):
        for accommodation in self:
            accommodation.totalbed_no = \
                accommodation.onebed_no + \
                accommodation.twobed_no + \
                accommodation.threebed_no + \
                accommodation.fourbed_no

    @api.multi
    def create_user(self):
        user_value = {
            'name': self.name,
            'login': self.email,
            'password': '123456',
            'active': True,
            'groups_id': [(6, False, [self.env.ref(
                'bms_guest_stay.group_e_guest_hotel_user').id])],
            'email': self.email,
            'street': self.street,
            'country_id': self.country_id.id,
            'state_id': self.state_id.id,
            'district_id': self.district_id.id,
            'ward_id': self.ward_id.id,
            'phone': self.phone,
            'mobile': self.mobile,
        }
        user_id = self.env['res.users'].create(user_value)
        self.write({'user_id': user_id.id})
        return user_id

        # action = self.env.ref('base.action_res_users')
        # action_read = action.read([])
        # action_read ['target'] = 'new'

    @api.multi
    def write(self, vals):
        fields_list = ['name', 'state_id', 'district_id', 'ward_id', 'website',
                       'phone', 'mobile']
        fields_list_change = []
        for field in vals.keys():
            if field in fields_list:
                fields_list_change.append(field)

        if fields_list_change:
            partner_vals = dict((f, vals[f]) for f in fields_list_change)
            self[0].user_id.partner_id.write(partner_vals)
        return super(BmsAccommodation, self).write(vals)
