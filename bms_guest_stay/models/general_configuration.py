# -*- coding: utf-8 -*-
from odoo import fields, models


class ReasonList(models.Model):
    _name = 'bms.guest.reason'

    reason_id = fields.Char()
    name = fields.Char(required=True)
    note = fields.Text()

    _sql_constraints = [
        ('reason_id_uniq', 'unique(reason_id)',
         'The reason_id must be unique !')
    ]


class BorderGateList(models.Model):
    _name = 'bms.guest.border.gate'

    border_gate_id = fields.Char()
    name = fields.Char(required=True)

    _sql_constraints = [
        ('border_gate_id_uniq', 'unique(border_gate_id)',
         'The border_gate_id must be unique !')
    ]
