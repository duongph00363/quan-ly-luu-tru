# -*- coding: utf-8 -*-
from odoo.addons.web.controllers import main
from odoo.http import request
import odoo
import odoo.modules.registry
from odoo.tools.translate import _
from odoo import http
import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import logging
_logger = logging.getLogger(__name__)
try:
    import httpagentparser
except (ImportError, IOError) as err:
    _logger.debug(err)


class Home(main.Home):
    @http.route('/web/login', type='http', auth="public")
    def web_login(self, redirect=None, **kw):
        main.ensure_db()
        request.params['login_success'] = False
        if request.httprequest.method == 'GET'\
                and redirect and request.session.uid:
            return http.redirect_with_hash(redirect)

        if not request.uid:
            request.uid = odoo.SUPERUSER_ID

        values = request.params.copy()
        try:
            values['databases'] = http.db_list()
        except odoo.exceptions.AccessDenied:
            values['databases'] = None

        if request.httprequest.method == 'POST':
            old_uid = request.uid
            uid = request.session.authenticate(
                request.session.db,
                request.params['login'],
                request.params['password'])
            if uid is not False:
                agent = request.httprequest.environ.get('HTTP_USER_AGENT')
                agent_details = httpagentparser.detect(agent)
                user_os = agent_details.get('os', {}).get('name', '')
                user_os_version = agent_details.get(
                    'os', {}).get('version', '')
                browser_name = agent_details.get('browser', {}).get('name', '')
                browser_version = agent_details.get(
                    'browser', {}).get('version', '')

                ip_address = request.httprequest.environ.get(
                    'HTTP_X_REAL_IP', '')

                browser_name = browser_name + ' ' + browser_version
                browser_name = browser_name.strip()

                user_os = user_os + ' ' + user_os_version
                user_os = user_os.strip()

                mac_address = ''

                vals = {
                    'user_id': uid,
                    'login_datetime':
                        datetime.datetime.now().strftime(
                            DEFAULT_SERVER_DATETIME_FORMAT),
                    'ip_address': ip_address,
                    'browser': browser_name,
                    'operation_system': user_os,
                    'mac_address': mac_address
                }

                request.env['bms.login.user.detail'].sudo().create(vals)

                request.params['login_success'] = True
                if not redirect:
                    redirect = '/web'
                return http.redirect_with_hash(redirect)
            request.uid = old_uid
            values['error'] = _("Wrong login/password")
        return request.render('web.login', values)
