# -*- coding: utf-8 -*-
from odoo import fields, models, api, exceptions, _
from odoo.tools.misc import DEFAULT_SERVER_DATE_FORMAT
import datetime
import pytz
from datetime import timedelta


class BmsLoginUserDetail(models.Model):
    _name = 'bms.login.user.detail'
    _description = 'Login user detail'
    _order = 'login_datetime desc'

    user_id = fields.Many2one('res.users', string='User',
                              required=True)
    unit_id = fields.Many2one('res.partner',
                              string='Unit')
    login_datetime = fields.Datetime(required=True)
    ip_address = fields.Char(required=True)
    browser = fields.Char()
    operation_system = fields.Char()
    mac_address = fields.Char()


class ViewLoginUserDetail(models.TransientModel):
    _name = 'view.login.user.detail'
    _description = 'View login user detail'

    unit_id = fields.Many2one('res.partner',
                              string='Unit')
    from_date = fields.Date(required=True)
    to_date = fields.Date(required=True)
    detail_ids = fields.One2many('bms.login.user.detail',
                                 compute='_compute_detail')

    @api.multi
    def _compute_detail(self):
        for view_detail in self:
            from_date = view_detail.from_date + ' 00:00:00'
            to_date = view_detail.to_date + ' 23:59:59'
            from_date = self.change_utc_to_local_datetime(from_date)
            to_date = self.change_utc_to_local_datetime(to_date)
            domain = [
                ('login_datetime', '>=', from_date),
                ('login_datetime', '<=', to_date)
            ]
            if view_detail.unit_id:
                domain.append(
                    ('unit_id', '=', view_detail.unit_id.id)
                )
            view_detail.detail_ids = \
                self.env['bms.login.user.detail'].search(domain)

    @api.multi
    def view_report(self):
        return True

    @api.multi
    @api.constrains('from_date', 'to_date')
    def _check_date(self):
        for wizard in self:
            from_date = wizard.from_date
            from_date = datetime.datetime.strptime(from_date,
                                                   DEFAULT_SERVER_DATE_FORMAT)
            to_date = wizard.to_date
            to_date = datetime.datetime.strptime(to_date,
                                                 DEFAULT_SERVER_DATE_FORMAT)

            if from_date > to_date:
                raise exceptions.ValidationError(_(
                    'From date must be less than or equal to date!'))
        return True

    @api.multi
    def change_utc_to_local_datetime(self, souce_date):
        user_tz = self.env.user.tz or 'Asia/Ho_Chi_Minh'
        tz_now = datetime.datetime.now(pytz.timezone(user_tz))
        difference = tz_now.utcoffset().total_seconds() / 60 / 60
        difference = int(difference)
        utc_date = datetime.datetime.strptime(souce_date,
                                              '%Y-%m-%d %H:%M:%S')
        utc_date = utc_date + timedelta(hours=-difference)
        return utc_date.strftime('%Y-%m-%d %H:%M:%S')
